'''
    Function module to provide an interface to the KIT Data Manager.

    It is required easywebdav (version 1.0.8).
    It can be downloaded from:
    https://github.com/amnong/easywebdav

    This file is part of click.
    click is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    click is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with click. If not, see <www.gnu.org/licenses/gpl.html>
'''

import os
from requests_oauthlib import OAuth1Session
from oauthlib.oauth1 import Client
import pkgutil, importlib, inspect
import xml.etree.ElementTree as etree
import time
from datetime import datetime, timedelta
import easywebdav
#import parser
from ConfigParser import SafeConfigParser
import json


__author__ = "Rossella Aversa"
__copyright__ = "Copyright 2017, CNR-IOM within the NFFA-Europe project. " \
                "NFFA-Europe has received funding from the EU's H2020 framework programe for " \
                "research and innovation under grant agreement n. 654360"
__credits__ = ["Rossella Aversa", "Stefano Piani", "Stefano Cozzini"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Rossella Aversa"
__email__ = "aversa@iom.cnr.it"
__status__ = "Prototype"

class XML_Error(Exception):
    pass

class NotFound_Error(Exception):
    pass

class Unauthorized_Error(Exception):
    pass

class ConnectionFailed(Exception):
    pass


class CustomClient(Client):
    def _render(self, request, formencode=False, realm=None):
        custom_user = 'REMOTE_USER' # This will remain the same
        custom_user_id = "c813b99e-a4ba-4c7a-af16-441003c8f796" # This must be changed to the ID of the PI
        request.headers[custom_user] = custom_user_id
        request.headers['Content-Type'] = 'application/json'
        request.headers['SITE_TOKEN'] = 'abcd-1234-edfg-5678'
        return super(CustomClient, self)._render(request, formencode, realm)

def read_settings(file):
    '''
        Function to provide all the information needed to authenticate
        to the KIT Data Manager installation (included webdav credentials
        and URL of the KIT DM interface, as well as token for IDRP interface).
        It reads a 'config.ini' file, and sets the credentials
    '''
    # RestUser credentials:
    config = SafeConfigParser()
    config.read(file)

    name = config.get('main','name')  # the name of the user
    group = config.get('main', 'group')  # the group of the user
    user_id = config.getint('main', 'user_id')  # the id of the user
    auth_id = config.get('main', 'auth_id')  # the authentication id of oauth
    cli_sec = config.get('main', 'cli_sec')  # the client secret
    res_own_key = config.get('main', 'res_own_key')  # the resourse owner key of oauth
    own_sec = config.get('main', 'own_sec') # the owner secret of oauth
    sig_met = config.get('main', 'sig_met')  # the kind of authentication
    kitdm_url = config.get('main', 'kitdm_url')  # KIT DM interface url
    webdav_user = config.get('main', 'webdav_user') # webdav username
    webdav_key = config.get('main', 'webdav_key') # webdav_key
    idrp_url = config.get('main', 'idrp_url') # IDRP interface url
    token = config.get('main', 'token') # IDRP token
    token_sec = config.get('main', 'token_sec') # IDRP token secret

    myself = RestUser(name, group, user_id, auth_id, cli_sec, res_own_key, own_sec, token, token_sec)
    webdav = [webdav_user, webdav_key]
    kit_rest = KitDMRestInterface(kitdm_url, myself)
    idrp_rest = IdrpClient(idrp_url, myself)

    return webdav, kit_rest, idrp_rest


def register_on_idrp(kit_rest, idrp_rest, digital_object_id, asset_type='PLAIN_DATA'):
    digital_object = kit_rest.get_digital_object(digital_object_id)
    investigation = digital_object.get_investigation()
    study = investigation.get_study()
    study_name = study.get_name()

    print "study ", study_name
    # look for proposal with the same number as in the name of the study.
    # This is possible only if the study name is written in the standard format
    # 'NFFA_Proposal_' + ID of the proposal

    if 'NFFA_Proposal_' in study_name:
        proposal = idrp_rest.get_proposal(study_name.lstrip('NFFA_Proposal_'))
    else:
        print("The selected digital object is in the study with ID {} named '{}'").format(study.get_id(), study_name)
        print('This name is not in the standard format, so it can not be automatically registered on the IDRP!')
        print('Please, do it manually.')
        exit()

    # create an experiment with the same metadata of the investigation and save its id
    investigation_name = investigation.get_name()
    investigation_start_time = investigation.get_start_date()
    if investigation_start_time[-5] == '.':
        investigation_start_time = investigation_start_time[:-5] + investigation_start_time[-1]
    if not __debug__:
        print investigation_start_time
    investigation_end_time = investigation.get_end_date()
    if investigation_end_time[-5] == '.':
        investigation_end_time = investigation_end_time[:-5] + investigation_end_time[-1]
    if not __debug__:
        print investigation_end_time

    # Before creating a new experiment, check if the same name already exists
    # if the experiment already exists, save its id
    # otherwise, create a new experiment

    experiments_list = proposal.get_experiments()

    #print "experiment list", experiments_list
    #print "investigation name", investigation_name

    for e in experiments_list:
        title = e["experimentTitle"]
        if investigation_name == title:
            experiment_id = e["experimentId"]
            break
    else:
        experiment_id = proposal.create_experiment(investigation_name,
                               start_Time=investigation_start_time,
                               end_Time=investigation_end_time,
                               experiment_Description='Experiment mapping the investigation with id ' + str(investigation.get_id())) #.get_id()




    # create a measurement with the same metadata of the digital object and save its id
    digital_object_name = digital_object.get_name()
    digital_object_start_time = digital_object.get_start_date()
    if digital_object_start_time[-5] == '.':
        digital_object_start_time = digital_object_start_time[:-5] + digital_object_start_time[-1]
    if not __debug__:
        print 'digital object start time: ', digital_object_start_time
    digital_object_end_time = digital_object.get_end_date()
    if digital_object_end_time[-5] == '.':
        digital_object_end_time = digital_object_end_time[:-5] + digital_object_end_time[-1]
    if not __debug__:
        print 'digital object end time: ', digital_object_end_time

    measurement_id = idrp_rest.get_experiment(str(experiment_id)).create_measurement('RAW',
                               digital_object_name,
                               measurement_description='Measurement mapping the digital object with id ' + str(digital_object_id),
                               start_time=digital_object_start_time,
                               end_time=digital_object_end_time).get_id()
    if not __debug__:
        print 'measurement id:', measurement_id

    #associate a LIST of at least one data asset to the measurement just created:
    data_asset_name = 'Data asset corresponding to ' + digital_object_name
    data_stream_identifier = kit_rest._url + 'dataorganization/organization/download/' + str(digital_object_id) + '/'

    data_asset = [[data_asset_name, data_stream_identifier, asset_type]]
    idrp_rest.get_measurement(measurement_id).assign_data_assets(data_asset)


def recursive_upload(local_dir, webdav):
    remote_files = [i.name for i in webdav.ls()]
    remote_dirs = [i.strip('/') for i in remote_files if i.endswith('/')]
    remote_dir_names = [os.path.basename(i) for i in remote_dirs]
    for file in os.listdir(local_dir):
        if os.path.isfile(os.path.join(local_dir,file)):
            webdav.upload(os.path.join(local_dir,file),file)
        elif os.path.isdir(os.path.join(local_dir,file)):
            if file not in remote_dir_names:
                webdav.mkdir(file)
            webdav.cd(file)
            new_local_dir = os.path.join(local_dir,file)
            recursive_upload(new_local_dir, webdav)
            webdav.cd('..')
        else:
            print('file {} ignored! '.format(os.path.join(local_dir,file)))


def recursive_children(kit_rest, endpoint, nodeId, group_id, list_tot, path = None,):
    queries = [('groupId', group_id)]

    new_endpoint = endpoint + str(nodeId) + '/children'
    # in principle, you should query for the maximum number of results,
    # but actually it shows already all the results
    new_xml_answer = kit_rest.raw_get(new_endpoint, queries)
    generic_wrapper = etree.fromstring(new_xml_answer)

    if not __debug__:
        print "------ NEW XML ANSWER -------"
        print new_xml_answer

    content = []

    for entities in generic_wrapper:
        try:
            content.append(read_entity(entities))
            if not __debug__:
                print 'content', content
        except XML_Error:
            pass

    for node in content:
        if node[2] is False:
            if path is not None:
                new_path = os.path.join(path, node[0])
                list_tot.append(new_path)
            else:
                list_tot.append(node[0])
        elif node[2] is True:
            if path is not None:
                new_path = os.path.join(path, node[0])
                list_tot.append(new_path + '/')
                if not __debug__:
                    print new_path, 'sub Directory'
                recursive_children(kit_rest, endpoint, node[1], group_id, list_tot, new_path)
            else:
                list_tot.append(node[0] + '/')
                if not __debug__:
                    print node[0], 'Directory'
                recursive_children(kit_rest, endpoint, node[1], group_id, list_tot, node[0])
        else:
            raise XML_Error('Error in reading the XML field: node')

    if not __debug__:
        print 'LIST OF FILES: \n'
        for i in list_tot:
            print i
        print 'DONE'

    return list_tot


def read_entity(entity):
    fname = None
    nodeId = None
    dir = None

    for child in entity:
        if child.tag == 'name':
            fname = child.text
            if not __debug__:
                print 'name = {}'.format(fname)

        if child.tag == 'nodeId':
            nodeId = child.text
            if not __debug__:
                print 'nodeId = {}'.format(nodeId)

        if child.tag == 'attributes':
            for attribute in child:
                for val in attribute:
                    if not __debug__:
                        if val.text == 'directory':
                            print val.text
                    if val.text == 'true':
                        if not __debug__:
                            print val.text
                        dir = True
                    elif val.text == 'false':
                        if not __debug__:
                            print val.text
                        dir = False
    if fname is None:
        raise XML_Error('Field name not found')
    if nodeId is None:
        raise XML_Error('Field nodeId not found')
    if dir is None:
        raise XML_Error('Field directory not found')
    return fname, nodeId, dir



class study_list(list):
    def get_id(self, n):
        id_list = [st.get_id() for st in self]
        return self[id_list.index(n)]

    def search_name(self, name):
        return study_list([st for st in self if st.get_name() == name])


class investigation_list(list):
    def get_id(self, n):
        id_list = [inv.get_id() for inv in self]
        return self[id_list.index(n)]

    def search_name(self, name):
        return investigation_list([inv for inv in self if inv.get_name() == name])


class digital_object_list(list):
    def get_id(self, n):
        id_list = [do.get_id() for do in self]
        return self[id_list.index(n)]

    def search_name(self, name):
        return digital_object_list([do for do in self if do.get_name() == name])


class access_point_list(list):
    def get_id(self, n):
        id_list = [ap.get_id() for ap in self]
        return self[id_list.index(n)]

    def search_name(self, name):
        return access_point_list([ap for ap in self if ap.get_name() == name])


# IDRP Client, needed to map a digital object to a data asset using the proposal metadata
class IdrpClient(object):
    def __init__(self, url, rest_user):
        if url[-1] != '/':
            url = url + '/'
        self._url = url
        self._rest_user = rest_user

    def get_url(self):
        ''' Return the URL of the IDRP client'''
        return self._url

    @staticmethod
    def rest_query_formatter(value_list):
        ''' Get a list of tuples like ((a,1),(b,None),(c,3)) and
        return a ReSTful query to append to an URL like ?a=1&c=3'''
        if value_list is None:
            return ''
        init_char = '?'
        output = ''
        for value in value_list:
            if value[1] != '' and value[1] is not None:
                output += init_char
                init_char = '&'
                output += str(value[0])
                output += "="
                output += str(value[1])
        return output

    def raw_get(self, endpoint, queries):
        '''
        This is a GET method to an arbitrary endpoint of the ReST API for the IDRP service.
        Endpoint is the location of the service; for example "project"

        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]

        So, for example, IdrpClient.raw_get('project',[('f','val1'),('g','val2')]) is a
        GET call to http://client_url/project?f1=val1&g=val2
        '''

        complete_url = self._url + endpoint
        complete_url += self.rest_query_formatter(queries)

        print "raw get to ", complete_url

        if not __debug__:
            print('GET Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        sig_met = self._rest_user._sig_met
        token = self._rest_user._token
        token_sec = self._rest_user._token_sec


        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=token,
                                   resource_owner_secret=token_sec,
                                   signature_method=sig_met,
                                   client_class=CustomClient
                                   )

        A = my_session.get(complete_url)
        if not __debug__:
            print "status code", A.status_code, "content", A.content
            print('GET Method:   Connection successful closed!')

        if A.status_code != 200:
            if A.status_code == 404:
                raise NotFound_Error('Not found')
            else: # to fix with all cases
                raise ConnectionFailed('Connection to {} failed with status {}'.format(complete_url, A.status_code))

        return A.content


    def raw_post(self, endpoint, queries, values_dict):
        '''
        This is a simple POST method to an arbitrary endpoint to the ReST interface.
        Endpoint is the location of the service; for example "project"

        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]

        So, for example, IdrpClient.raw_get('project',[('f','val1'),('g','val2')]) is a
        GET call to http://client_url/project?f1=val1&g=val2
        '''

        complete_url = self._url + endpoint
        complete_url += self.rest_query_formatter(queries)

        print "complete url", complete_url

        if not __debug__:
            print('POST Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        token = self._rest_user._token
        token_sec = self._rest_user._token_sec
        sig_met = self._rest_user._sig_met

        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=token,
                                   resource_owner_secret=token_sec,
                                   signature_method=sig_met,
                                   client_class=CustomClient
                                   )

        if not __debug__:
            print('Posting the following: ' + str(values_dict))

        A = my_session.post(complete_url, values_dict)
        print 'A ', A

        if not __debug__:
            print A.status_code
            print('POST Method:   Connection successful closed!')

        return A.content.decode("utf-8")


    def raw_put(self, endpoint, queries, values_dict):
        '''
        This is a PUT method to an arbitrary endpoint to the ReST interface.
        Endpoint is the location of the service; for example "project"

        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]

        So, for example, IdrpClient.raw_get('project',[('f','val1'),('g','val2')]) is a
        GET call to http://client_url/project?f1=val1&g=val2
        '''

        complete_url = self._url + endpoint
        complete_url += self.rest_query_formatter(queries)

        if not __debug__:
            print('PUT Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        sig_met = self._rest_user._sig_met
        token = self._rest_user._token
        token_sec = self._rest_user._token_sec

        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=token,
                                   resource_owner_secret=token_sec,
                                   signature_method=sig_met,
                                   client_class=CustomClient
                                   )
        if not __debug__:
            print('Putting the following: ' + str(values_dict))

        A = my_session.put(complete_url, values_dict)

        if not __debug__:
            print('PUT Method:   Connection successful closed!')

        return A.content.decode("utf-8")


    def _read_list(self, endpoint, queries, results_at_once=100):
        '''Read a list of simple values from the KIT Rest interface

        Raises: XML_Error and errors generated by etree.parse()'''
        count = results_at_once  # Just a start value; during the process
        # this variable will store how many
        # results had been returned
        cycles = 0
        id_list = []  # The list of the results
        queries_original_list = queries[:]

        while count == results_at_once:
            queries_list = queries_original_list[:]
            queries_list.append(('first', results_at_once * cycles))
            queries_list.append(('results', results_at_once))

            json_answer = self.raw_get(endpoint, queries_list)
            id_list = json.loads(json_answer)

            cycles += 1
        return id_list


    def _parse_proposal_json(self, json_answer):
        prop_dict = json.loads(json_answer)
        return prop_dict

    def _get_proposal_dict(self, proposal_id):
        proposal_id = str(proposal_id)
        endpoint = 'proposals/' + proposal_id
        queries = []
        return self._parse_proposal_json(self.raw_get(endpoint, queries))

    def get_proposal(self, proposal_id):
        '''Get a proposal by id'''
        return self.proposal(self._get_proposal_dict(proposal_id), self)

    def _parse_experiment_json(self, json_answer):
        print "json_answer", json_answer
        exp_dict = json.loads(json_answer)
        return exp_dict

    def _get_experiment_dict(self, experiment_id):
        experiment_id = str(experiment_id)
        endpoint = 'experiments/' + experiment_id
        queries = []
        return self._parse_experiment_json(self.raw_get(endpoint, queries))

    def get_experiment(self, experiment_id):
        '''Get the experiment with the provided id'''
        exp_dict = self._get_experiment_dict(experiment_id)
        return self.experiment(exp_dict, self)

    def _parse_measurement_json(self, json_answer):
        meas_dict = json.loads(json_answer)
        return meas_dict

    def _get_measurement_dict(self, measurement_id):
        measurement_id = str(measurement_id)
        endpoint = 'measurements/' + measurement_id
        queries = []
        return self._parse_experiment_json(self.raw_get(endpoint, queries))

    def get_measurement(self, measurement_id):
        '''Get the measurement with the provided id'''
        meas_dict = self._get_measurement_dict(measurement_id)
        return self.measurement(meas_dict, self)

    def _parse_data_asset_json(self, json_answer):
        da_dict = json.loads(json_answer)
        return da_dict

    def _get_data_asset_dict(self, measurement_id, data_asset_id):
        measurement_id = str(measurement_id)
        data_asset_id = str(data_asset_id)
        endpoint = 'measurements/' + measurement_id + '/dataAsset/' + data_asset_id
        queries = []
        return self._parse_data_asset_json(self.raw_get(endpoint, queries))

    def get_data_asset(self, measurement_id, data_asset_id):
        '''Get the data asset with the provided id, related to a measurement with the provided id'''
        da_dict = self._get_data_asset_dict(measurement_id, data_asset_id)
        return self.data_asset(da_dict, self)


    class proposal():
        '''
        A proposal describes a list of at least two experiments caried out in at least
        two different nano-facilities. Each proposal is part of the project.
        '''

        def __init__(self, proposal_dict, idrp_rest):
            self.__proposal_dict = dict()
            self.__proposal_dict["proposalId"] = None,
            self.__proposal_dict["projectId"] = None,
            self.__proposal_dict["members"] = [],
            self._read_dict(proposal_dict)
            self.__idrp_rest = idrp_rest

        def _read_dict(self, proposal_dict):
            if 'proposalId' in proposal_dict:
                self.__proposal_dict['proposalId'] = proposal_dict['proposalId']
            else:
                raise ValueError('ProposalId not found!')
            if 'projectId' in proposal_dict:
                self.__proposal_dict["projectId"] = str(proposal_dict["projectId"])
            else:
                raise ValueError('projectId not found!')
            if 'members' in proposal_dict:
                if proposal_dict['members'] is not None:
                    member_list = [i for i in proposal_dict['members']]
                    self.__proposal_dict['members'] = member_list
                else:
                    self.__proposal_dict['members'] = list()
            else:
                raise ValueError('members not found!')

        def _update_self(self):
            prop_dict = self.__idrp_rest._get_proposal_dict(self.get_id())
            self._read_dict(prop_dict)

        def get_dict(self):
            return self.__proposal_dict

        def get_id(self):
            '''Return the id of the proposal'''
            return self.__proposal_dict['proposalId']

        def get_experiments(self, results_at_once=100):
            '''Return all the experiments which belong to this proposal.'''
            proposal_id = self.get_id()
            endpoint = 'proposals/' + proposal_id + '/experiments'
            queries = [('results',results_at_once)]
            json_answer = self.__idrp_rest.raw_get(endpoint, queries)
            exp_list = json.loads(json_answer)
            return exp_list

        def create_experiment(self,
                              experiment_Title,
                              start_Time=None,
                              end_Time=None,
                              experiment_Description=None):

            proposal_id = self.get_id()
            print "proposal id", proposal_id

            if start_Time is None:
                start_Time = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
            else:
                start_Time = start_Time.rstrip()

            if end_Time is None:
                oneyear = timedelta(days=365)
                end_Time = (datetime.now()+oneyear).strftime("%Y-%m-%dT%H:%M:%SZ")  # One year

            if end_Time <= start_Time:
                raise ValueError('The end date must be after the start date!')

            endpoint = 'experiments'
            queries = []

            post_values = {
                "proposalId": proposal_id,
                "experimentTitle": experiment_Title,
                "startTime":start_Time,
                "endTime":end_Time,
                "experimentDescription": experiment_Description,
            }

            print "post values", post_values

            json_response = self.__idrp_rest.raw_post(endpoint, queries, post_values)

            print "json response", json_response

            exp_dict = self.__idrp_rest._parse_experiment_json(json_response)
            self._update_self()


            return IdrpClient.experiment(exp_dict, self.__idrp_rest)


    class experiment():
        '''
        An experiment described an activity of investigating/producing
        samples or data assets with a defined start and end.
        An experiment is linked to a proposal.
        '''

        def __init__(self, experiment_dict, idrp_rest):
            self.__experiment_dict = dict()
            self.__experiment_dict["proposalId"] = None,
            self.__experiment_dict["experimentId"] = None,
            self.__experiment_dict["experimentIdentifier"] = None,
            self.__experiment_dict["experimentTitle"] = None,
            self.__experiment_dict["startTime"] = None,
            self.__experiment_dict["endTime"] = None,
            self.__experiment_dict["experimentDescription"] = None
            self._read_dict(experiment_dict)
            self.__idrp_rest = idrp_rest

        def _read_dict(self, experiment_dict):
            if 'proposalId' in experiment_dict:
                self.__experiment_dict['proposalId'] = str(experiment_dict['proposalId'])
            else:
                raise ValueError('ProposalId not found!')
            if 'experimentId' in experiment_dict:
                self.__experiment_dict['experimentId'] = str(experiment_dict['experimentId'])
            else:
                raise ValueError('experimentId not found!')
            if 'experimentIdentifier' in experiment_dict:
                self.__experiment_dict['experimentIdentifier'] = str(experiment_dict['experimentIdentifier'])
            else:
                raise ValueError('experimentIdentifier not found!')
            if 'experimentTitle' in experiment_dict:
                self.__experiment_dict['experimentTitle'] = str(experiment_dict['experimentTitle'])
            else:
                raise ValueError('experimentTitle not found!')
            if 'startTime' in experiment_dict:
                self.__experiment_dict['startTime'] = str(experiment_dict['startTime'])
            else:
                raise ValueError('startTime not found!')
            if 'endTime' in experiment_dict:
                self.__experiment_dict['endTime'] = experiment_dict['endTime']
            else:
                raise ValueError('endTime not found!')
            if 'experimentDescription' in experiment_dict:
                self.__experiment_dict['experimentDescription'] = experiment_dict['experimentDescription']
            else:
                raise ValueError('experimentDescription not found!')

        def _update_self(self):
            exp_dict = self.__idrp_rest._get_experiment_dict(self.get_id())
            self._read_dict(exp_dict)

        def get_id(self):
            '''Return the experiment Id'''
            return self.__experiment_dict['experimentId']

        def get_proposal(self):
            '''Return the proposal Id this experiment belongs to'''
            return self.__idrp_rest.get_proposal(self.__experiment_dict['proposalId'])

        def get_measurements(self, results_at_once=100):
            '''Return a list containing a subset of all measurements that are part of a specific experiment.'''
            experiment_id = self.get_id()
            endpoint = 'experiments/' + experiment_id + '/measurements'
            queries = [('results',results_at_once)]
            json_answer = self.__idrp_rest.raw_get(endpoint, queries)
            meas_list = json.loads(json_answer)
            return meas_list

        def create_measurement(self,
                               measurement_type,
                               measurement_name,
                               measurement_description=None,
                               start_time=None,
                               end_time=None,
                               embargo=None,
                               sample_id=None,
                               instrument_id=None):

            experiment_id = self.get_id()
            print "experiment id ", experiment_id

            if str(measurement_type) is not 'RAW' and str(measurement_type) is not 'ANALYZED':
                raise Exception('measurement type must be either RAW or ANALYZED')

            if start_time is None:
                start_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")

            if end_time is None:
                oneyear = timedelta(days=365)
                end_time = (datetime.now()+oneyear).strftime("%Y-%m-%dT%H:%M:%SZ")

            if end_time <= start_time:
                raise ValueError('The end date must be after the start date!')

            if embargo is None:
                twoyears = timedelta(days=365*2)
                embargo = (datetime.now()+twoyears).strftime("%Y-%m-%dT%H:%M:%SZ")
            else:
                embargo = embargo

            endpoint = 'measurements'
            queries = []

            post_values = {
                "experimentId": experiment_id,
                "measurementType": str(measurement_type),
                "measurementName": measurement_name,
                "measurementDescription": measurement_description,
                "measurementStart": start_time,
                "measurementEnd": end_time,
                "embargoUntil": embargo,
                "sampleId": sample_id,
                "instrumentId": instrument_id
            }

            print "post values ", post_values
            json_response = self.__idrp_rest.raw_post(endpoint, queries, post_values)
            meas_dict = self.__idrp_rest._parse_experiment_json(json_response)
            self._update_self()
            return IdrpClient.measurement(meas_dict, self.__idrp_rest)


    class measurement():
        '''
        A measurement describes the act of a data collection. A measurement can also be
        a simulation of an analysis, therefore, an instrumentId can also refer to a
        software package. It may use a sample as input, which can represent a model,
        a configuration, or data input. The result of the measurement is a data asset
        that is stored in an archive.
        '''

        def __init__(self, measurement_dict, idrp_rest):
            self.__measurement_dict = dict()
            self.__measurement_dict['experimentId'] = None
            self.__measurement_dict['measurementId'] = None
            self.__measurement_dict['measurementType'] = None
            self.__measurement_dict['measurementName'] = None
            self.__measurement_dict['measurementDescription'] = None
            self.__measurement_dict['measurementStart'] = None
            self.__measurement_dict['measurementEnd'] = None
            self.__measurement_dict['embargoUntil'] = None
            self.__measurement_dict['sampleId'] = None
            self.__measurement_dict['instrumentId'] = None
            self._read_dict(measurement_dict)
            self.__idrp_rest = idrp_rest

        def _read_dict(self, measurement_dict):
            if 'experimentId' in measurement_dict:
                self.__measurement_dict['experimentId'] = str(measurement_dict['experimentId'])
            else:
                raise ValueError('experimentId not found!')
            if 'measurementId' in measurement_dict:
                self.__measurement_dict['measurementId'] = str(measurement_dict['measurementId'])
            else:
                raise ValueError('measurementId not found!')
            if 'measurementType' in measurement_dict:
                self.__measurement_dict['measurementType'] = str(measurement_dict['measurementType'])
            else:
                raise ValueError('measurementType not found!')
            if 'measurementName' in measurement_dict:
                self.__measurement_dict['measurementName'] = str(measurement_dict['measurementName'])
            else:
                raise ValueError('measurementName not found!')
            if 'measurementDescription' in measurement_dict:
                self.__measurement_dict['measurementDescription'] = str(measurement_dict['measurementDescription'])
            else:
                raise ValueError('measurementDescription not found!')
            if 'measurementStart' in measurement_dict:
                self.__measurement_dict['measurementStart'] = str(measurement_dict['measurementStart'])
            else:
                raise ValueError('measurementStart not found!')
            if 'measurementEnd' in measurement_dict:
                self.__measurement_dict['measurementEnd'] = str(measurement_dict['measurementEnd'])
            else:
                raise ValueError('measurementEnd not found!')
            if 'embargoUntil' in measurement_dict:
                self.__measurement_dict['embargoUntil'] = str(measurement_dict['embargoUntil'])
            else:
                raise ValueError('embargoUntil not found!')
            if 'sampleId' in measurement_dict:
                self.__measurement_dict['sampleId'] = str(measurement_dict['sampleId'])
            else:
                raise ValueError('sampleId not found!')
            if 'instrumentId' in measurement_dict:
                self.__measurement_dict['instrumentId'] = str(measurement_dict['instrumentId'])
            else:
                raise ValueError('instrumentId not found!')

        def _update_self(self):
            meas_dict = self.__idrp_rest._get_measurement_dict(self.get_id())
            self._read_dict(meas_dict)

        def get_id(self):
            '''Return the measurement Id'''
            return self.__measurement_dict['measurementId']

        def get_experiment(self):
            '''Return the experiment Id this measurement belongs to'''
            return self.__idrp_rest.get_experiment(self.__measurement_dict['experimentId'])

        def get_data_assets(self, results_at_once=100):
            '''Returns a list containing a subset of all data assets that are part of a specific measurement.'''
            measurement_id = self.get_id()
            endpoint = 'measurements/' + measurement_id + '/dataAssets'
            queries = [('results',results_at_once)]
            json_answer = self.__idrp_rest.raw_get(endpoint, queries)
            da_list = json.loads(json_answer)
            return da_list

        def get_data_asset(self, data_asset_id):
            '''Get the data asset with the provided Id'''
            measurement_id = self.get_id()
            data_asset_id = str(data_asset_id)
            endpoint = 'measurements/' + measurement_id + '/dataAssets/' + data_asset_id
            queries = []
            #json_answer = self.__idrp_rest.raw_get(endpoint, queries)
            da_dict = self.__idrp_rest._get_data_asset_dict(measurement_id, data_asset_id)
            return IdrpClient.data_asset(da_dict, self.__idrp_rest)

        def create_data_asset(self,
                              data_asset_name,
                              data_stream_identifier,
                              asset_type,
                              data_archive_id="generic-archive-1",
                              data_format = None,
                              data_format_identifier = None,
                              data_type = None,
                              data_size = 0,
                              date_of_collection = None,
                              intellectual_property_rights = None):
            ''' Create a data asset. This function is called by the function assign_data_assets(data_asset_list)
                for each data asset you want to associate to a measurement (inserted into a
                list of at least 1 element)
            '''

            measurement_id = str(self.get_id())
            asset_type = str(asset_type)
            data_asset_name = str(data_asset_name)
            data_stream_identifier = str(data_stream_identifier)
            data_archive_id = str(data_archive_id)

            if asset_type is not 'PLAIN_DATA' and asset_type is not 'LANDING_PAGE' and asset_type is not 'PUBLICATION':
                raise Exception('data asset type must be PLAIN_DATA, or LANDING_PAGE, or PUBLICATION')

            if date_of_collection is None:
                date_of_collection = self.__measurement_dict['measurementStart'] # date of the measurement

            post_values = {
                "assetType": asset_type,
                "measurementId": measurement_id,
                "dataArchiveId": data_archive_id,
                "dataAssetName": data_asset_name,
                "dataStreamIdentifier": data_stream_identifier,
                "dataFormat": data_format,
                "dataFormatIdentifier": data_format_identifier,
                "dataType": data_type,
                "dataSize": data_size,
                "dateOfCollection": date_of_collection,
                "intellectualPropertyRights": intellectual_property_rights
            }
            return post_values

        def assign_data_assets(self, data_asset_list):
            '''Assing a data asset to a measurement'''

            measurement_id = str(self.get_id())
            endpoint = 'measurements/' + measurement_id + '/dataAssets'
            queries = []

            post_val_list = []
            for da in data_asset_list:
                post_val_list.append(self.create_data_asset(da[0],da[1],da[2]))

            json_values = json.dumps(post_val_list)
            self.__idrp_rest.raw_post(endpoint, queries, json_values)
            return



    class data_asset():
        '''A data asset is a combination of data units which can be Raw Data (including a result
        of computer simulation), Analyzed Data, or Data Analyses (configurations or/and logs of
        Data Analyses execution). A data asset is the outcome of a measurement.
        With this endpoint a JSON representation of a DataAsset array can be associated with a measurement
        identified by its id. As a result, an internal ingest process is triggered performing all further steps.
        Access to the data assets is available as soon as the ingest process has finished, which may take
        a while depending on the complexity of the process. To check whether the ingest process has been
        finished, the user may call /{measurementId}/dataAssets/count as long as this call returns 0.'''

        def __init__(self, data_asset_dict, idrp_rest):
            self.__data_asset_dict = dict()
            self.__data_asset_dict['assetType'] = None
            self.__data_asset_dict['measurementId'] = None
            self.__data_asset_dict['dataArchiveId'] = None
            self.__data_asset_dict['dataAssetId'] = None
            self.__data_asset_dict['dataAssetIdentifier'] = None
            self.__data_asset_dict['dataAssetName'] = None
            self.__data_asset_dict['dataStreamIdentifier'] = None
            self.__data_asset_dict['dataFormat'] = None
            self.__data_asset_dict['dataFormatIdentifier'] = None
            self.__data_asset_dict['dataType'] = None
            self.__data_asset_dict['dataSize'] = None
            self.__data_asset_dict['dataChecksum'] = None
            self.__data_asset_dict['dateOfCollection'] = None
            self.__data_asset_dict['intellectualPropertyRights'] = None
            self._read_dict(data_asset_dict)
            self.__idrp_rest = idrp_rest

        def _read_dict(self, data_asset_dict):
            if 'assetType' in data_asset_dict:
                self.__data_asset_dict['assetType'] = str(data_asset_dict['assetType'])
            else:
                raise ValueError('assetType not found!')
            if 'measurementId' in data_asset_dict:
                self.__data_asset_dict['measurementId'] = str(data_asset_dict['measurementId'])
            else:
                raise ValueError('measurementId not found!')
            if 'dataArchiveId' in data_asset_dict:
                self.__data_asset_dict['dataArchiveId'] = str(data_asset_dict['dataArchiveId'])
            else:
                raise ValueError('dataArchiveId not found!')
            if 'dataAssetId' in data_asset_dict:
                self.__data_asset_dict['dataAssetId'] = str(data_asset_dict['dataAssetId'])
            else:
                raise ValueError('dataAssetId not found!')
            if 'dataAssetName' in data_asset_dict:
                self.__data_asset_dict['dataAssetName'] = str(data_asset_dict['dataAssetName'])
            else:
                raise ValueError('dataAssetName not found!')
            if 'dataAssetIdentifier' in data_asset_dict:
                self.__data_asset_dict['dataAssetIdentifier'] = str(data_asset_dict['dataAssetIdentifier'])
            else:
                raise ValueError('dataAssetIdentifier not found!')
            if 'dataAssetName' in data_asset_dict:
                self.__data_asset_dict['dataAssetName'] = str(data_asset_dict['dataAssetName'])
            else:
                raise ValueError('dataAssetName not found!')
            if 'dataStreamIdentifier' in data_asset_dict:
                self.__data_asset_dict['dataStreamIdentifier'] = str(data_asset_dict['dataStreamIdentifier'])
            else:
                raise ValueError('dataStreamIdentifier not found!')
            if 'dataFormat' in data_asset_dict:
                self.__data_asset_dict['dataFormat'] = str(data_asset_dict['dataFormat'])
            else:
                raise ValueError('dataFormat not found!')
            if 'dataFormatIdentifier' in data_asset_dict:
                self.__data_asset_dict['dataFormatIdentifier'] = str(data_asset_dict['dataFormatIdentifier'])
            else:
                raise ValueError('dataFormatIdentifier not found!')
            if 'dataType' in data_asset_dict:
                self.__data_asset_dict['dataType'] = str(data_asset_dict['dataType'])
            else:
                raise ValueError('dataType not found!')
            if 'dataSize' in data_asset_dict:
                self.__data_asset_dict['dataSize'] = str(data_asset_dict['dataSize'])
            else:
                raise ValueError('dataSize not found!')
            if 'dataChecksum' in data_asset_dict:
                self.__data_asset_dict['dataChecksum'] = str(data_asset_dict['dataChecksum'])
            else:
                raise ValueError('dataChecksum not found!')
            if 'dateOfCollection' in data_asset_dict:
                self.__data_asset_dict['dateOfCollection'] = str(data_asset_dict['dateOfCollection'])
            else:
                raise ValueError('dateOfCollection not found!')
            if 'intellectualPropertyRights' in data_asset_dict:
                self.__data_asset_dict['intellectualPropertyRights'] = str(data_asset_dict['intellectualPropertyRights'])
            else:
                raise ValueError('intellectualPropertyRights not found!')

        def _update_self(self):
            da_dict = self.__idrp_rest._get_data_asset_dict(self.get_id())
            self._read_dict(da_dict)

        def get_id(self):
            '''Return the data asset Id'''
            return self.__data_asset_dict['dataAssetId']

        def get_measurement(self):
            '''Return the measurement Id this data asset is associated to'''
            return self.__idrp_rest.get_measurement(self.__data_asset_dict['measurementId'])


class RestUser(object):
    '''An object of the RestUser class include all the information needed to
    provide to a KitDMRestInterface all the information needed to authenticate
    to a KIT Data Manager installation.'''

    def __init__(self, name, group, user_id, auth_id, cli_sec, res_own_key,
                 own_sec, token, token_sec, sig_met='PLAINTEXT'):
        '''Create a RestUser object from the following data:
        - the name of the user
        - the group of the user
        - the id of the user
        - the authentication id of oauth
        - the cli_sec
        - the resource owner key of oauth
        - the owner secret of oauth
        - the owner token of oauth
        - the owner token secret of oauth
        - the kind of authentication
        '''
        self._name = name
        self._group = group
        self._id = int(user_id)
        self._auth_id = auth_id
        self._cli_sec = cli_sec
        self._res_own_key = res_own_key
        self._res_own_sec = own_sec
        self._token = token
        self._token_sec = token_sec
        self._sig_met = sig_met

    def get_name(self):
        ''' Return the name of the user'''
        return self._name

    def get_group(self):
        ''' Return the group of the user'''
        return self._group

    def get_id(self):
        ''' Return the id of the user'''
        return self._id


class KitDMRestInterface(object):
    def __init__(self, url, rest_user):
        if url[-1] != '/':
            url = url + '/'
        self._url = url
        self._rest_user = rest_user

    @staticmethod
    def rest_query_formatter(value_list):
        ''' Get a list of tuples like ((a,1),(b,None),(c,3)) and
        return a restful query to append to an URL like ?a=1&c=3'''
        if value_list is None:
            return ''
        init_char = '?'
        output = ''
        for value in value_list:
            if value[1] != '' and value[1] is not None:
                output += init_char
                init_char = '&'
                output += str(value[0])
                output += "="
                output += str(value[1])
        return output

    def raw_get(self, endpoint, queries):
        '''This is a simple GET method to an arbitrary endpoint to the ReST interface.

        Endpoint is the location of the service; for example "staging"
        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]

        So, for example, rest_interface.raw_get('staging',[('f','val1'),('g','val2')]) is a
        GET call to http://rest_url/staging?f1=val1&g=val2'''

        complete_url = self._url + endpoint
        complete_url += KitDMRestInterface.rest_query_formatter(queries)

        if not __debug__:
            print('GET Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        sig_met = self._rest_user._sig_met

        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=res_own_key,
                                   resource_owner_secret=res_own_sec,
                                   signature_method=sig_met
                                   )

        A = my_session.get(complete_url)
        if not __debug__:
            print "status code", A.status_code, "content", A.content
        if not __debug__:
            print('GET Method:   Connection successful closed!')

        if A.status_code != 200:
            raise ConnectionFailed('Connection to {} failed with status {}'.format(complete_url, A.status_code))

        return A.content #.decode("utf-8")

    def raw_post(self, endpoint, queries, values_dict):
        '''This is a simple POST method to an arbitrary endpoint to the ReST interface.

        Endpoint is the location of the service; for example "staging"
        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]
        Values dict is the content of the POST method.'''

        complete_url = self._url + endpoint
        complete_url += KitDMRestInterface.rest_query_formatter(queries)

        if not __debug__:
            print('POST Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        sig_met = self._rest_user._sig_met

        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=res_own_key,
                                   resource_owner_secret=res_own_sec,
                                   signature_method=sig_met
                                   )
        if not __debug__:
            print('   Posting the following: ' + str(values_dict))

        A = my_session.post(complete_url, values_dict)

        if not __debug__:
            print('POST Method:   Connection successful closed!')

        return A.content.decode("utf-8")

    def raw_put(self, endpoint, queries, values_dict):
        '''This is a simple PUT method to an arbitrary endpoint to the ReST interface.

        Endpoint is the location of the service; for example "staging"
        Queries must be a list of pairs of values like in rest_query_formatter, for example
        [('f','val1'),('g','val2')]
        Values dict is the content of the POST method.'''

        complete_url = self._url + endpoint
        complete_url += KitDMRestInterface.rest_query_formatter(queries)

        if not __debug__:
            print('PUT Method:   Connecting to ' + complete_url)

        auth_id = self._rest_user._auth_id
        cli_sec = self._rest_user._cli_sec
        res_own_key = self._rest_user._res_own_key
        res_own_sec = self._rest_user._res_own_sec
        sig_met = self._rest_user._sig_met

        my_session = OAuth1Session(auth_id,
                                   client_secret=cli_sec,
                                   resource_owner_key=res_own_key,
                                   resource_owner_secret=res_own_sec,
                                   signature_method=sig_met
                                   )
        if not __debug__:
            print('   Putting the following: ' + str(values_dict))

        A = my_session.put(complete_url, values_dict)

        if not __debug__:
            print('PUT Method:   Connection successful closed!')

        return A.content.decode("utf-8")

    def _read_list(self, endpoint, queries, results_at_once=100):
        '''Read a list of simple values from the KIT Rest interface

        Raises: XML_Error and errors generated by etree.parse()'''
        count = results_at_once  # Just a start value; during the process
        # this variable will store how many
        # results had been returned
        cycles = 0
        id_list = []  # The list of the results
        queries_original_list = queries[:]

        while count == results_at_once:
            queries_list = queries_original_list[:]
            queries_list.append(('first', results_at_once * cycles))
            queries_list.append(('results', results_at_once))

            xml_answer = self.raw_get(endpoint, queries_list)

            generic_wrapper = etree.fromstring(xml_answer)
            not_found_count = True
            for child in generic_wrapper:
                if child.tag == 'count':
                    not_found_count = False
                    count = int(child.text)
                elif child.tag == 'entities':
                    for entry in child:
                        id_list.append(int(entry[0].text))

            if not_found_count:
                raise XML_Error('The entry "count" is missing!')
            cycles += 1
        return id_list

    def get_access_point_id_list(self, group_id=None):
        if group_id is None:
            group_id = self._rest_user.get_group()
        endpoint = 'staging/accesspoints/'
        return self._read_list(endpoint, [('groupId', group_id)])

    def get_staging_processor_id_list(self, group_id=None):
        if group_id is None:
            group_id = self._rest_user.get_group()
        endpoint = 'staging/processors/'
        return  self._read_list(endpoint, [('groupId', group_id)])

    class study():
        ''' An object that carries all of the information about a specific
        study inside the KIT Data Manager.

        Please, do not try to create such an object by hand, because it needs
        a specific dictionary retrieved from an xml answer of the KIT Data
        manager. Use instead the function get_study(), for example.'''

        def _read_dict(self, study_dict):
            if 'studyId' in study_dict:
                self.__study_dict['studyId'] = int(study_dict['studyId'])
            else:
                raise ValueError('StudyId not found!')
            if 'userId' in study_dict:
                self.__study_dict['userId'] = int(study_dict['userId'])
            else:
                raise ValueError('UserId not found!')
            if 'topic' in study_dict:
                self.__study_dict['topic'] = study_dict['topic']
            else:
                raise ValueError('Study name not found!')
            if 'note' in study_dict:
                self.__study_dict['note'] = study_dict['note']
            if 'legalNote' in study_dict:
                self.__study_dict['legalNote'] = study_dict['legalNote']
            if 'startDate' in study_dict:
                self.__study_dict['startDate'] = study_dict['startDate']
            if 'endDate' in study_dict:
                self.__study_dict['endDate'] = study_dict['endDate']
            if 'investigations' in study_dict:
                if study_dict['investigations'] is not None:
                    st_id_list = [int(i) for i in study_dict['investigations']]
                    self.__study_dict['investigations'] = st_id_list
                else:
                    self.__study_dict['investigations'] = list()

        def __init__(self, study_dict, kit_rest, group_id):
            self.__study_dict = dict()
            self.__study_dict['studyId'] = 0
            self.__study_dict['userId'] = 0
            self.__study_dict['topic'] = None
            self.__study_dict['note'] = None
            self.__study_dict['legalNote'] = None
            self.__study_dict['startDate'] = None
            self.__study_dict['endDate'] = None
            self.__study_dict['investigations'] = []
            self._read_dict(study_dict)
            self.__kit_rest = kit_rest
            self.__group_access = group_id

        def _update_self(self):
            st_dict = self.__kit_rest._get_study_dict(self.get_id(),
                                                      self.__group_access)
            self._read_dict(st_dict)

        def get_dict(self):
            return self.__study_dict

        def get_id(self):
            '''Return the id of the study'''
            return self.__study_dict['studyId']

        def get_name(self):
            '''Return the name (also called topic) of the study'''
            return self.__study_dict['topic']

        def get_note(self):
            '''Return the contents of the note field of the study'''
            return self.__study_dict['note']

        def get_legal_note(self):
            '''Return the contents of the legal note field of the study'''
            return self.__study_dict['legalNote']

        def get_start_date(self):
            '''Return a human readable string containing date and
            time of when the study had been started'''
            return self.__study_dict['startDate']

        def get_end_date(self):
            '''Return a human readable string containing date and
            time of when the study will end'''
            return self.__study_dict['endDate']

        def get_investigations(self):
            '''Return all the investigations which belong to this study.'''
            inv_list = []
            for inv_id in self.__study_dict['investigations']:
                inv_list.append(self.__kit_rest.get_investigation(inv_id, self.__group_access))
            return investigation_list(inv_list)

        def create_investigation(self,
                                 investigation_name,
                                 group_id=None,
                                 note=None,
                                 description=None,
                                 start_date=None,
                                 end_date=None):
            '''Create an investigation inside the current study and return it.
            start_date and end_date are milliseconds from unix epoc.'''

            # Sanitizing input
            study_id = self.__study_dict['studyId']

            if group_id is None:
                group_id = self.__kit_rest._rest_user.get_group()

            if start_date is None:
                start_date = int(time.time() * 1000)
            else:
                start_date = int(start_date)

            if end_date is None:
                end_date = start_date + 36 * 10 ** 5 * 24 * 365  # One year
            else:
                end_date = int(end_date)

            if end_date <= start_date:
                raise ValueError('The end date must be after the start date!')
            ###

            endpoint = 'basemetadata/studies/' + str(study_id) + '/investigations'
            queries = [('groupId', group_id)]

            post_values = {
                "topic": investigation_name,
                "note": note,
                "description": description,
                "startDate": start_date,
                "endDate": end_date
            }

            xml_response = self.__kit_rest.raw_post(endpoint, queries, post_values)

            inv_dict = self.__kit_rest._parse_investigation_xml(xml_response)

            self._update_self()

            return KitDMRestInterface.investigation(inv_dict, self.__kit_rest, group_id)

        def __repr__(self):
            return 'study_' + str(self.__study_dict['studyId'])

        def __str__(self):
            output = ''
            for field in ['studyId', 'topic', 'userId', 'note', 'legalNote',
                          'startDate', 'endDate', 'investigations', 'investigations name']:
                if field == 'investigations name':
                    value = [i.get_name() for i in self.get_investigations()]
                else:
                    value = self.__study_dict[field]
                output += '{:<27}'.format(field) + str(value) + '\n'
            return output

    def _parse_study_xml(self, xml_answer):
        try:
            studyWrapper = etree.fromstring(xml_answer)
        except etree.ParseError:
            if 'HTTP Status 404 - Not Found' in xml_answer:
                raise NotFound_Error('The studyId was nonexistent '
                                     'or the URL was wrong')
            elif 'HTTP Status 401 - Unauthorized' in xml_answer:
                raise Unauthorized_Error('You are not allowed to perform '
                                         'this operation!')
            else:
                raise

        study_dict = dict()
        for child in studyWrapper:
            if child.tag == 'count':
                if int(child.text) != 1:
                    raise XML_Error('XML returns ' + child.text + ' as count'
                                                                  ' number. The answer should be 1.')
            if child.tag == 'entities':
                if len([i for i in child]) != 1:
                    raise XML_Error('XML has multiple entries, while only'
                                    ' one was espected.')
                entry = child[0]
                inv_list = []
                for field in entry:
                    if len(field) == 0:
                        study_dict[field.tag] = field.text
                    elif field.tag == 'manager':
                        userId_entry = field[0]
                        study_dict[userId_entry.tag] = userId_entry.text
                    elif field.tag == 'investigations':
                        for invest in field:
                            #                            if len(invest) != 1 :
                            #                                raise XML_Error('Investigations should be described '
                            #                                                'only by their id')
                            #                            if invest[0].tag != 'investigationId':
                            #                                raise XML_Error('Investigations should be described '
                            #                                                'only by their id')
                            #                            inv_list.append(int(invest[0].text))
                            for invest_field in invest:
                                if invest_field.tag == 'investigationId':
                                    inv_list.append(int(invest_field.text))
                        study_dict['investigations'] = inv_list
        return study_dict

    def create_study(self,
                     study_name,
                     group_id=None,
                     manager_user_id=None,
                     start_date=None,
                     end_date=None,
                     note='',
                     legal_note=""):
        '''Create a study and return it as a study object.
        start_date and end_date are milliseconds from unix epoc.'''
        # Sanitizing input
        if group_id is None:
            group_id = self._rest_user.get_group()

        if manager_user_id is None:
            manager_user_id = 1  ### THIS SHALL BE FIXED!!!
        else:
            manager_user_id = int(manager_user_id)

        if start_date is None:
            start_date = int(time.time() * 1000)
        else:
            start_date = int(start_date)

        if end_date is None:
            end_date = start_date + 36 * 10 ** 5 * 24 * 365  # One year
        else:
            end_date = int(end_date)

        if end_date <= start_date:
            raise ValueError('The end date must be after the start date!')
        ###

        endpoint = 'basemetadata/studies'
        queries = [('groupId', group_id)]

        post_values = {
            "topic": study_name,
            "note": note,
            "legalNote": legal_note,
            "managerUserId": manager_user_id,
            "startDate": start_date,
            "endDate": end_date
        }

        xml_response = self.raw_post(endpoint, queries, post_values)

        return KitDMRestInterface.study(self._parse_study_xml(xml_response), self, group_id)

    def _get_study_dict(self, study_id, group_id=None):
        study_id = int(study_id)
        endpoint = 'basemetadata/studies/' + str(study_id)
        if group_id is None:
            group_id = self._rest_user.get_group()
        queries = [('groupId', group_id)]
        return self._parse_study_xml(self.raw_get(endpoint, queries))

    def get_study(self, study_id, group_id=None):
        '''Get a study by id'''
        if group_id is None:
            group_id = self._rest_user.get_group()
        return self.study(self._get_study_dict(study_id, group_id), self, group_id)

    def get_studyid_list(self, group_id=None):
        if group_id is None:
            group_id = self._rest_user.get_group()
        endpoint = 'basemetadata/studies'
        return self._read_list(endpoint, [('groupId', group_id)])

    def get_study_list(self, group_id=None):
        '''Get a list of all the available studies'''
        if group_id is None:
            group_id = self._rest_user.get_group()
        id_list = self.get_studyid_list(group_id)
        st_list = study_list()
        for i in id_list:
            st_list.append(self.get_study(i, group_id))
        return st_list

    class investigation():
        ''' An object that carries all of the information about a specific
        investigation inside the KIT Data Manager.

        Please, do not try to create such an object by hand, because it needs
        a specific dictionary retrieved from an xml answer of the KIT Data
        manager. Use instead the function get_investigation(), for example.'''

        def _read_dict(self, inv_dict):
            if 'investigationId' in inv_dict:
                self.__inv_dict['investigationId'] = int(inv_dict['investigationId'])
            else:
                raise ValueError('InvestigationId not found!')
            if 'topic' in inv_dict:
                self.__inv_dict['topic'] = inv_dict['topic']
            else:
                raise ValueError('Investigation name not found!')
            if 'note' in inv_dict:
                self.__inv_dict['note'] = inv_dict['note']
            if 'description' in inv_dict:
                self.__inv_dict['description'] = inv_dict['description']
            if 'startDate' in inv_dict:
                self.__inv_dict['startDate'] = inv_dict['startDate']
            if 'endDate' in inv_dict:
                self.__inv_dict['endDate'] = inv_dict['endDate']
            if 'studyId' in inv_dict:
                self.__inv_dict['studyId'] = inv_dict['studyId']
            else:
                raise ValueError('Parent studyId not found!')
            if 'participants' in inv_dict:
                if inv_dict['participants'] is not None:
                    par_list = [int(i) for i in inv_dict['participants']]
                    self.__inv_dict['participants'] = par_list
                else:
                    self.__inv_dict['participants'] = list()
            if 'digitalObjects' in inv_dict:
                if inv_dict['digitalObjects'] is not None:
                    do_list = [int(i) for i in inv_dict['digitalObjects']]
                    self.__inv_dict['digitalObjects'] = do_list
                else:
                    self.__inv_dict['digitalObjects'] = list()

        def __init__(self, inv_dict, kit_rest, group_id):
            self.__inv_dict = dict()
            self.__inv_dict['investigationId'] = 0
            self.__inv_dict['topic'] = None
            self.__inv_dict['note'] = None
            self.__inv_dict['description'] = None
            self.__inv_dict['studyId'] = 0
            self.__inv_dict['startDate'] = None
            self.__inv_dict['endDate'] = None
            self.__inv_dict['participants'] = []
            self.__inv_dict['digitalObjects'] = []
            self._read_dict(inv_dict)
            self.__kit_rest = kit_rest
            self.__group_access = group_id

        def _update_self(self):
            inv_dict = self.__kit_rest._get_investigation_dict(self.get_id(),
                                                               self.__group_access)
            self._read_dict(inv_dict)

        def __get_dict(self):
            return self.__inv_dict

        def get_id(self):
            '''Return the id of the investigation'''
            return self.__inv_dict['investigationId']

        def get_name(self):
            '''Return the name (also called topic) of the investigation'''
            return self.__inv_dict['topic']

        def get_note(self):
            '''Return the contents of the field note of the investigation'''
            return self.__inv_dict['note']

        def get_description(self):
            '''Return the contents of the field description of
               the investigation'''
            return self.__inv_dict['description']

        def get_start_date(self):
            '''Return a human readable string containing date and
               time of when the investigation had been started'''
            return self.__inv_dict['startDate']

        def get_end_date(self):
            '''Return a human readable string containing date and
               time of when the investigation will end'''
            return self.__inv_dict['endDate']

        def get_study(self, groupId=None):
            '''Return the study this investigation belongs to'''
            if groupId is None:
                groupId = self.__group_access
            return self.__kit_rest.get_study(self.__inv_dict['studyId'],
                                             groupId)

        def get_digital_objects(self):
            '''Return a list of all the digital objects that are inside this
            investigation.'''
            do_list = [self.__kit_rest.get_digital_object(i, self.__group_access)
                       for i in self.__inv_dict['digitalObjects']]
            return digital_object_list(do_list)

        def create_digital_object(self, digital_object_name, group_id=None,
                                  uploader_id=None, note=None, start_date=None,
                                  end_date=None):
            '''Create a digital object inside the current investigation and
            return it. start_date and end_date are milliseconds from unix
            epoc.'''

            # Sanitizing input
            inv_id = self.__inv_dict['investigationId']

            if group_id is None:
                group_id = self.__kit_rest._rest_user.get_group()

            if uploader_id is None:
                uploader_id = self.__kit_rest._rest_user.get_id()

            if start_date is None:
                start_date = int(time.time() * 1000)
            else:
                start_date = int(start_date)

            if end_date is None:
                end_date = start_date + 36 * 10 ** 5 * 24 * 365  # One year
            else:
                end_date = int(end_date)

            if end_date <= start_date:
                raise ValueError('The end date must be after the start date!')

            if note is None:
                note = ''
            ###

            endpoint = ('basemetadata/investigations/' +
                        str(inv_id) + '/digitalObjects')
            queries = [('groupId', group_id)]

            post_values = {
                "label": digital_object_name,
                "note": note,
                "uploaderId": uploader_id,
                "startDate": start_date,
                "endDate": end_date
            }

            xml_response = self.__kit_rest.raw_post(endpoint, queries, post_values)

            do_dict = self.__kit_rest._parse_digital_object_xml(xml_response)

            self._update_self()
            do_obj = KitDMRestInterface.digital_object(do_dict, self.__kit_rest,
                                                       self.__group_access)

            return do_obj

        def __repr__(self):
            return 'investigation_' + str(self.__inv_dict['investigationId'])

        def __str__(self):
            output = ''
            for field in ['investigationId', 'topic', 'studyId', 'studyName', 'description',
                          'note', 'startDate', 'endDate', 'digitalObjects',
                          'participants']:
                if field == 'studyName':
                    value = self.get_study().get_name()
                else:
                    value = str(self.__inv_dict[field])
                output += '{:<27}'.format(field) + str(value) + '\n'
            return output

    def _parse_investigation_xml(self, xml_answer):
        try:
            investigationWrapper = etree.fromstring(xml_answer)
        except etree.ParseError:
            if 'HTTP Status 404 - Not Found' in xml_answer:
                raise NotFound_Error('The investigationId was nonexistent '
                                     'or the URL was wrong')
            elif 'HTTP Status 401 - Unauthorized' in xml_answer:
                raise Unauthorized_Error('You are not allowed to perform '
                                         'this operation!')
            else:
                raise

        inv_dict = dict()
        for child in investigationWrapper:
            if child.tag == 'count':
                if int(child.text) != 1:
                    raise XML_Error('XML returns ' + child.text + ' as count'
                                                                  ' number. The answer should be 1.')
            if child.tag == 'entities':
                if len([i for i in child]) != 1:
                    raise XML_Error('XML has multiple entries, while only'
                                    ' one was expected.')
                entry = child[0]
                for field in entry:
                    if len(field) == 0:
                        inv_dict[field.tag] = field.text
                    elif field.tag == 'participants':
                        part_list = []
                        for participant in field:
                            if len(participant) != 1 or participant[0].tag != 'participantId':
                                raise XML_Error('Error reading investigation participants')
                            part_list.append(participant[0].text)
                        inv_dict['participants'] = part_list
                    elif field.tag == 'digitalObjects':
                        do_list = []
                        for do in field:
                            #                            if len(do) != 1 or do[0].tag != 'baseId':
                            #                                raise XML_Error('Error reading digital objects of this investigation')
                            for do_field in do:
                                if do_field.tag == 'baseId':
                                    do_list.append(int(do_field.text))
                        inv_dict['digitalObjects'] = do_list
                    elif field.tag == 'study':
                        study_id = int(field[0].text)
                        inv_dict['studyId'] = study_id

        return inv_dict

    def _get_investigation_dict(self, investigation_id, group_id=None):
        investigation_id = int(investigation_id)
        endpoint = 'basemetadata/investigations/' + str(investigation_id)
        if group_id is None:
            group_id = self._rest_user.get_group()
        queries = [('groupId', group_id)]
        return self._parse_investigation_xml(self.raw_get(endpoint, queries))

    def get_investigation(self, investigation_id, group_id=None):
        '''Get an investigation by id'''
        if group_id is None:
            group_id = self._rest_user.get_group()
        inv_dict = self._get_investigation_dict(investigation_id, group_id)
        return self.investigation(inv_dict, self, group_id)

    class digital_object():
        '''An object that carries all of the information about a specific
        digital object inside the KIT Data Manager.

        Please, do not try to create such an object by hand, because it needs
        a specific dictionary retrieved from an xml answer of the KIT Data
        manager. Use instead the implemented functions.'''

        def _read_dict(self, do_dict):
            if 'baseId' in do_dict:
                self.__do_dict['baseId'] = int(do_dict['baseId'])
            else:
                raise ValueError('baseId not found!')
            if 'digitalObjectIdentifier' in do_dict:
                self.__do_dict['digitalObjectIdentifier'] = do_dict['digitalObjectIdentifier']
            else:
                raise ValueError('digitalObjectIdentifier not found!')
            if 'investigationId' in do_dict:
                self.__do_dict['investigationId'] = int(do_dict['investigationId'])
            else:
                raise ValueError('Parent investigationId not found!')
            if 'label' in do_dict:
                self.__do_dict['label'] = do_dict['label']
            else:
                raise ValueError('Digital Object name not found!')
            if 'note' in do_dict:
                self.__do_dict['note'] = do_dict['note']
            if 'startDate' in do_dict:
                self.__do_dict['startDate'] = do_dict['startDate']
            if 'endDate' in do_dict:
                self.__do_dict['endDate'] = do_dict['endDate']
            if 'uploadDate' in do_dict:
                self.__do_dict['uploadDate'] = do_dict['uploadDate']
            if 'uploader' in do_dict:
                if do_dict['uploader'] is not None:
                    self.__do_dict['uploader'] = [int(i) for i in do_dict['uploader']]
                else:
                    self.__do_dict['uploader'] = list()
            if 'experimenters' in do_dict:
                if do_dict['experimenters'] is not None:
                    self.__do_dict['experimenters'] = [int(i) for i in do_dict['experimenters']]
                else:
                    self.__do_dict['experimenters'] = list()

        def __init__(self, do_dict, kit_rest, group_id):
            self.__do_dict = dict()
            self.__do_dict['baseId'] = 0
            self.__do_dict['digitalObjectIdentifier'] = 0
            self.__do_dict['investigationId'] = None
            self.__do_dict['label'] = None
            self.__do_dict['note'] = None
            self.__do_dict['startDate'] = None
            self.__do_dict['uploadDate'] = None
            self.__do_dict['endDate'] = None
            self.__do_dict['uploader'] = []
            self.__do_dict['experimenters'] = []
            self._read_dict(do_dict)
            self.__kit_rest = kit_rest
            self.__group_access = group_id

        def _update_self(self):
            do_dict = self.__kit_rest._get_digital_object_dict(self.get_id(),
                                                               self.__group_access)
            self._read_dict(do_dict)

        def get_id(self):
            '''Return the digital object Id'''
            return self.__do_dict['baseId']

        def get_uuid(self):
            '''Return the digitalObjectIdentifier'''
            return self.__do_dict['digitalObjectIdentifier']

        def get_name(self):
            '''Return the name of the digital object'''
            return self.__do_dict['label']

        def get_start_date(self):
            '''Return the start date of the digital object'''
            return self.__do_dict['startDate']

        def get_end_date(self):
            '''Return the end date of the digital object'''
            return self.__do_dict['endDate']

        def get_investigation(self):
            '''Return the investigation in which this digital object is embedded'''
            return self.__kit_rest.get_investigation(
                self.__do_dict['investigationId'], self.__group_access)

        def get_ingest_id(self):
            '''Returns the id of the ingest associated to the data object, if scheduled.
            Otherwise, returns none'''
            endpoint = 'staging/ingests'
            queries = [('objectId', self.get_uuid())]

            xml_answer = self.__kit_rest.raw_get(endpoint, queries)

            if not __debug__:
                print "------ XML ANSWER -------"
                print xml_answer

            generic_wrapper = etree.fromstring(xml_answer)

            for child in generic_wrapper:
                if child.tag == 'count':
                    count = int(child.text)
                    #print(count)
                    break
            else:
                raise Exception('Field count not found')

            if count != 0 and count != 1:
                raise Exception('Invalid value for count: ' + str(count))
            if count == 0:
                return None

            id_field = list(generic_wrapper.iter('id'))
            if len(id_field) == 0:
                raise Exception('Field count not found')
            elif len(id_field) > 1:
                raise Exception('Multiple IDs available')

            unique_field = id_field[0]
            return int(unique_field.text)

        def get_download_id(self):
            '''Returns the id of the download associated to the data object.
            Useful both for download and for list'''
            endpoint = 'staging/downloads'
            queries = [('objectId', self.get_uuid())]

            xml_answer = self.__kit_rest.raw_get(endpoint, queries)
            generic_wrapper = etree.fromstring(xml_answer)

            for child in generic_wrapper:
                if child.tag == 'count':
                    count = int(child.text)
                    break
                else:
                    raise Exception('Field count not found')

            if count != 0 and count != 1:
                raise Exception('Invalid value for count: ' + str(count))
            if count == 0:
                return None

            id_field = list(generic_wrapper.iter('id'))
            if len(id_field) == 0:
                raise Exception('Field count not found')
            elif len(id_field) > 1:
                raise Exception('Multiple IDs available')

            unique_field = id_field[0]
            return int(unique_field.text)

        def get_ingest_url(self):
            '''Returns the URL for the upload of a file'''
            ingest_id = self.get_ingest_id()
            endpoint = 'staging/ingests/' + str(ingest_id)

            if ingest_id is None:
                raise Exception('Ingest ID not found')

            xml_answer = self.__kit_rest.raw_get(endpoint, [])
            generic_wrapper = etree.fromstring(xml_answer)

            url_field = list(generic_wrapper.iter('stagingUrl'))
            if len(url_field) == 0:
                raise Exception('Field stagingUrl not found')
            elif len(url_field) > 1:
                raise Exception('Multiple stagingUrl fields available')

            stagingUrl = url_field[0]
            return str(stagingUrl.text)

        def get_download_url(self):
            '''Returns the URL for the TEMPORARY created download.
            This is NOT the url for direct download.
            See download_file or download_zip'''
            download_id = self.get_download_id()
            endpoint = 'staging/downloads/' + str(download_id)

            if download_id is None:
                raise Exception('Download ID not found')

            xml_answer = self.__kit_rest.raw_get(endpoint, [])
            generic_wrapper = etree.fromstring(xml_answer)

            url_field = list(generic_wrapper.iter('stagingUrl'))
            if len(url_field) == 0:
                raise Exception('Field stagingUrl not found')
            elif len(url_field) > 1:
                raise Exception('Multiple stagingUrl fields available')

            stagingUrl = url_field[0]
            return str(stagingUrl.text)


        def upload_file(self, local_file, remote_file, username, password):
            url = self.get_ingest_url() + 'data'
            protocol = 'http'
            path = ''
            # look for the first occurrence of the string '//'
            start_url = url.find('//')
            if start_url != -1:
                protocol = url[:start_url].rstrip(':')
                url = url[start_url:].lstrip('/')  # remove '//'

            end_url = url.find('/')
            if end_url != -1:
                path = url[end_url:].rstrip('/') # remove '/'
                url = url[:end_url]

            port = 80
            start_port = url.find(':')
            if start_port != -1:
                port = int(url[start_port+1:])
                url = url[:start_port]

            #print(url, '\n', path, '\n', port, '\n', protocol

            webdav = easywebdav.connect(url,
                                        path=path,
                                        protocol=protocol,
                                        port=port,
                                        username=username,
                                        password=password)

            webdav.upload(local_file, remote_file)
            print("uploaded file ", local_file, " to ", os.path.join(path, remote_file))


        def download_file(self, remote_file, local_file):
            #direct download a single file in the digital object
            object_id = self.get_id()
            ingest_id = self.get_ingest_id()

            if ingest_id is None:
                raise Exception('Ingest ID is none for this object!')
            endpoint = 'dataorganization/organization/download/' + str(object_id) + '/' + str(remote_file)

            try:
                file = self.__kit_rest.raw_get(endpoint, [])
            except:
                #print(self.__kit_rest.raw_get(endpoint, []))
                raise Exception('File {} not found in digital object {}'.format(remote_file, self.get_id()))
            print('downloading file {} to {}'.format(remote_file, local_file))
            with open(str(local_file), 'wb') as f:
                f.write(file)


        def upload_all(self, local_dir, username, password):
            url = self.get_ingest_url() + 'data'
            protocol = 'http'
            path = ''
            # look for the first occurrence of the string '//'
            start_url = url.find('//')
            if start_url != -1:
                protocol = url[:start_url].rstrip(':')
                url = url[start_url:].lstrip('/')  # remove '//'

            end_url = url.find('/')
            if end_url != -1:
                path = url[end_url:].rstrip('/')  # remove '/'
                url = url[:end_url]

            port = 80
            start_port = url.find(':')
            if start_port != -1:
                port = int(url[start_port + 1:])
                url = url[:start_port]

            if not __debug__:
                print(url, '\n', path, '\n', port, '\n', protocol)

            webdav = easywebdav.connect(url,
                                        path=path,
                                        protocol=protocol,
                                        port=port,
                                        username=username,
                                        password=password)


            recursive_upload(local_dir, webdav)
            if not __debug__:
                print("--------- DONE! ---------")


        def download_zip(self, local_dir):
            #direct download the content of the digital object as a zip file
            object_id = self.get_id()
            ingest_id = self.get_ingest_id()

            if ingest_id is None:
                raise Exception('Ingest ID is none for this object!')
            endpoint = 'dataorganization/organization/download/' + str(object_id) + '/'
            print('Downloading digital object {} in {}'.format(object_id, local_dir))
            zip = self.__kit_rest.raw_get(endpoint, [])
            with open(str(local_dir), 'wb') as f:
                f.write(zip)


        def list_files(self, group_id = None):
            # return a list of files within the digital object by looking for the children of the node
            baseId = self.get_id()

            endpoint = 'dataorganization/organization/' + str(baseId) + '/'
            queries = [('groupId', group_id)]

            ingest_id = self.get_ingest_id()

            if ingest_id is None:
                raise Exception('No ingest scheduled for this object!')

            if group_id is None:
                group_id = self.__group_access
            xml_answer = self.__kit_rest.raw_get(endpoint, queries)
            generic_wrapper = etree.fromstring(xml_answer)
            if not __debug__:
                print('XML ANSWER -->', xml_answer)

            for child in generic_wrapper.iter():
                if child.tag == 'nodeId':
                    nodeId = int(child.text)
                    if not __debug__:
                        print('nodeId', nodeId)
                    break
            else:
                raise Exception('Field nodeId not found')

            list_total = []
            recursive_children(self.__kit_rest, endpoint, nodeId, group_id, list_total)
            return list_total


        def schedule_ingest(self, access_point, group_id=None):
            '''NB: when scheduling an ingest, the status goes to 4 (PRE_INGEST_SCHEDULED)'''
            endpoint = 'staging/ingests'
            queries = [('groupId', group_id)]

            if group_id is None:
                group_id = self.__group_access

            post_values = {
                "objectId": self.get_uuid(),
                "accessPoint": access_point.get_uuid(),
                "stagingProcessors":'[]'
            }

            ingest_id = self.get_ingest_id()
            if ingest_id is not None:
                raise Exception('An ingest has been already scheduled for this object!')

            xml_answer = self.__kit_rest.raw_post(endpoint, queries, post_values)
            return xml_answer

        def close_ingest(self):
            ingest_id = self.get_ingest_id()
            if ingest_id is None:
                raise Exception('No ingest scheduled for this object')
            endpoint = 'staging/ingests/' + str(ingest_id)
            # change the status to PRE_INGEST_FINISHED
            self.__kit_rest.raw_put(endpoint, [], {'status': 16})

        def create_download(self, access_point, group_id=None):
            endpoint = 'staging/downloads/'
            queries = [('groupId', group_id)]
            if group_id is None:
                group_id = self.__group_access

            post_values = {
                'objectId': self.get_uuid(),
                'accessPoint': access_point.get_uuid(),
                #'dataOrganizationTree':'{}',
                'stagingProcessors': '[]'
            }

            download_id = self.get_download_id()
            ingest_id = self.get_ingest_id()
            if download_id is None:
                if ingest_id is None:
                    raise Exception('Never ingested to object {}!'.format(self.get_id()))
                xml_answer = self.__kit_rest.raw_post(endpoint, queries, post_values)
                if not __debug__:
                    print(xml_answer)
                return xml_answer


        def __repr__(self):
            return 'digital_object_' + str(self.get_uuid())

        def __str__(self):
            output = ''
            for field in ['baseId', 'digitalObjectIdentifier', 'investigationId', 'investigation',
                'studyId', 'study', 'label', 'note', 'startDate', 'uploadDate', 'endDate',
                          'uploader', 'experimenters']:
                if field == 'investigation':
                    value = self.get_investigation().get_name()
                elif field == 'studyId':
                    value = self.get_investigation().get_study().get_id()
                elif field == 'study':
                    value = self.get_investigation().get_study().get_name()
                else:
                    value = str(self.__do_dict[field])
                output += '{:<27}'.format(field) + str(value) + '\n'
            return output

        def get_ingest_status(self):
            ingest_id = self.get_ingest_id()

            if ingest_id is None:
                raise Exception('No ingest scheduled for this object')

            endpoint = 'staging/ingests/' + str(ingest_id)
            xml_answer = self.__kit_rest.raw_get(endpoint, [])

            generic_wrapper = etree.fromstring(xml_answer)
            status_field = list(generic_wrapper.iter('status'))
            if len(status_field) == 0:
                raise Exception('Field status not found')
            elif len(status_field) > 1:
                raise Exception('Multiple status fields available')

            unique_field = status_field[0]
            if not __debug__:
                print unique_field
            return int(unique_field.text)


        def get_download_status(self):

            download_id = self.get_download_id()
            ingest_id = self.get_ingest_id()

            if download_id is None:
                if ingest_id is None:
                    print('This object has never had an ingest')
                    exit()
                print('No download ID for this object')
                exit()

            endpoint = 'staging/downloads/' + str(download_id)
            xml_answer = self.__kit_rest.raw_get(endpoint, [])

            generic_wrapper = etree.fromstring(xml_answer)
            status_field = list(generic_wrapper.iter('status'))
            if len(status_field) == 0:
                raise Exception('Field status not found')
            elif len(status_field) > 1:
                raise Exception('Multiple status fields available')

            unique_field = status_field[0]
            return int(unique_field.text)


        def wait_status(self):
            status = self.get_download_status()
            if status == 2:
                raise Exception('preparation failed')
            elif status == 8:
                raise Exception('download removed')
            i = 0
            imax = 30
            while i < imax:
                if self.get_download_status() != 4:
                    print('status is {}. processing, please wait...'.format(self.get_download_status()))
                    time.sleep(5)
                    i += 1
                else:
                    break


    def _parse_digital_object_xml(self, xml_answer):
        try:
            digitalObjectWrapper = etree.fromstring(xml_answer)
        except etree.ParseError:
            if 'HTTP Status 404 - Not Found' in xml_answer:
                raise NotFound_Error('The digital object Id was nonexistent '
                                     'or the URL was wrong')
            elif 'HTTP Status 401 - Unauthorized' in xml_answer:
                raise Unauthorized_Error('You are not allowed to perform '
                                         'this operation!')
            else:
                raise

        do_dict = dict()
        for child in digitalObjectWrapper:
            if child.tag == 'count':
                if int(child.text) != 1:
                    raise XML_Error('XML returns ' + child.text + ' as count'
                                                                  ' number. The answer should be 1.')
            if child.tag == 'entities':
                if len([i for i in child]) != 1:
                    raise XML_Error('XML has multiple entries, while only'
                                    ' one was expected.')
                entry = child[0]
                for field in entry:
                    if len(field) == 0:
                        do_dict[field.tag] = field.text
                    elif field.tag == 'experimenters':
                        exp_list = []
                        for experimenter in field:
                            if len(experimenter) != 1 or experimenter[0].tag != 'userId':
                                raise XML_Error('XML error reading experimenters!')
                            exp_list.append(int(experimenter[0].text))
                        do_dict['experimenters'] = exp_list
                    elif field.tag == 'uploader':
                        up_list = []
                        for uploader in field:
                            up_list.append(uploader.text)
                        do_dict['uploader'] = up_list
                    elif field.tag == 'investigation':
                        investigation_id = int(field[0].text)
                        do_dict['investigationId'] = investigation_id
        return do_dict

    def _get_digital_object_dict(self, digital_object_id, group_id=None):
        digital_object_id = int(digital_object_id)
        endpoint = 'basemetadata/digitalObjects/' + str(digital_object_id)
        if group_id is None:
            group_id = self._rest_user.get_group()
        queries = [('groupId', group_id)]
        return self._parse_digital_object_xml(self.raw_get(endpoint, queries))

    def get_digital_object(self, do_id, group_id=None):
        '''Get a digital object by id'''
        if group_id is None:
            group_id = self._rest_user.get_group()
        do_dict = self._get_digital_object_dict(do_id, group_id)
        return self.digital_object(do_dict, self, group_id)


    class staging_processor(object):
        def _read_dict(self, sp_dict):
            if 'id' in sp_dict:
                self.__sp_dict['id'] = int(sp_dict['id'])
            else:
                raise ValueError('id not found')
            for field in ['id','uniqueIdentifier','name','properties','priority','disabled',
                          'implementationClass','description','groupId','downloadProcessingSupported',
                          'defaultOn','ingestProcessingSupported']:
                if field in sp_dict:
                    self.__sp_dict[field] = sp_dict[field]

        def __init__(self, sp_dict, kit_rest, group_id):
            self.__sp_dict = dict()
            self.__sp_dict['id'] = 0
            self.__sp_dict['uniqueIdentifier'] = None
            self.__sp_dict['name'] = None
            self.__sp_dict['properties'] = None
            self.__sp_dict['priority'] = None
            self.__sp_dict['disabled'] = None
            self.__sp_dict['implementationClass'] = None
            self.__sp_dict['description'] = None
            self.__sp_dict['groupId'] = 0
            self.__sp_dict['downloadProcessingSupported'] = None
            self.__sp_dict['defaultOn'] = None
            self.__sp_dict['ingestProcessingSupported'] = None




    class remote_access_point(object):
        def _read_dict(self, am_dict):
            if 'id' in am_dict:
                self.__am_dict['id'] = int(am_dict['id'])
            else:
                raise ValueError('id not found!')
            for field in ['groupId', 'remoteBaseUrl', 'defaultAccessPoint'
                                                      'description', 'uniqueIdentifier', 'name',
                          'customProperties', 'implementationClass',
                          'transientAccessPoint', 'disabled', 'localBasePath']:
                if field in am_dict:
                    self.__am_dict[field] = am_dict[field]

        def __init__(self, am_dict, kit_rest, group_id):
            self.__am_dict = dict()
            self.__am_dict['id'] = 0
            self.__am_dict['groupId'] = 0
            self.__am_dict['remoteBaseUrl'] = None
            self.__am_dict['defaultAccessPoint'] = None
            self.__am_dict['description'] = None
            self.__am_dict['uniqueIdentifier'] = None
            self.__am_dict['name'] = None
            self.__am_dict['customProperties'] = None
            self.__am_dict['implementationClass'] = None
            self.__am_dict['transientAccessPoint'] = None
            self.__am_dict['disabled'] = None
            self.__am_dict['localBasePath'] = None
            self._read_dict(am_dict)
            self.__kit_rest = kit_rest
            self.__group_access = group_id

        #            if self.__class__ == remote_access_point:  # @UndefinedVariable
        #                raise NotImplementedError('Try to create a remote_access_point, '
        #                                'but this is an abstract class')

        def get_id(self):
            '''Return the access method'''
            return self.__am_dict['id']

        def get_name(self):
            '''Return the name of the access method'''
            return self.__am_dict['name']

        def get_uuid(self):
            '''Return the unique identifier'''
            return self.__am_dict['uniqueIdentifier']

        def __repr__(self):
            return 'access_point_' + str(self.__am_dict['id'])

    def _parse_access_point_xml(self, xml_answer):
        try:
            access_point_wrapper = etree.fromstring(xml_answer)
        except etree.ParseError:
            if 'HTTP Status 404 - Not Found' in xml_answer:
                raise NotFound_Error('The digital object Id was nonexistent '
                                     'or the URL was wrong')
            elif 'HTTP Status 401 - Unauthorized' in xml_answer:
                raise Unauthorized_Error('You are not allowed to perform '
                                         'this operation!')
            else:
                raise

        am_dict = dict()
        for child in access_point_wrapper:
            if child.tag == 'count':
                if int(child.text) != 1:
                    raise XML_Error('XML returns ' + child.text + ' as count'
                                                                  ' number. The answer should be 1.')
            if child.tag == 'entities':
                if len([i for i in child]) != 1:
                    raise XML_Error('XML has multiple entries, while only'
                                    ' one was expected.')
                entry = child[0]
                for field in entry:
                    if len(field) == 0:
                        am_dict[field.tag] = field.text
                    else:
                        raise ValueError('Unknown field ' + str(field))
        return am_dict

    def _get_access_point_dict(self, access_point_id, group_id=None):
        access_point_id = int(access_point_id)
        endpoint = 'staging/accesspoints/' + str(access_point_id)
        if group_id is None:
            group_id = self._rest_user.get_group()
        queries = [('groupId', group_id)]
        return self._parse_access_point_xml(self.raw_get(endpoint, queries))

    def get_access_point(self, access_point_id, group_id=None):
        access_point_id = int(access_point_id)
        am_dict = self._get_access_point_dict(access_point_id, group_id=None)
        return KitDMRestInterface.remote_access_point(am_dict, self, group_id)
        #am_dict['implementationClass'] = 'Webdav'
        #if 'implementationClass' in am_dict:
        #    pass
        #else:
        #    raise ValueError('ImplementationClass is not reported!')

    def get_access_point_list(self, group_id=None):
        '''Get a list of all the available access points'''
        if group_id is None:
            group_id = self._rest_user.get_group()
        id_list = self.get_access_point_id_list(group_id)
        ap_list = access_point_list()
        for i in id_list:
            ap_list.append(self.get_access_point(i, group_id))
        return ap_list

    class ingest():
        def _read_dict(self, ingest_dict):
            if 'status' in ingest_dict:
                self.__ingest_dict['status'] = int(ingest_dict['status'])
            else:
                raise (ValueError, 'status not found for the ingest')
            if 'id' in ingest_dict:
                self.__ingest_dict['id'] = int(ingest_dict['id'])
            else:
                raise (ValueError, 'id not found for the ingest')
            if 'lastUpdate' in ingest_dict:
                self.__ingest_dict['lastUpdate'] = int(ingest_dict['lastUpdate'])
            if 'expiresAt' in ingest_dict:
                self.__ingest_dict['expiresAt'] = int(ingest_dict['expiresAt'])
            if 'digitalObjectUuid' in ingest_dict:
                self.__ingest_dict['digitalObjectUuid'] = ingest_dict['digitalObjectUuid']
            else:
                raise (ValueError, 'digitalObjectUuid not found for the ingest')
            if 'ownerUuid' in ingest_dict:
                self.__ingest_dict['ownerUuid'] = ingest_dict['ownerUuid']
            else:
                raise (ValueError, 'ownerUuid not found for the ingest')
            if 'accessPointId' in ingest_dict:
                self.__ingest_dict['accessPointId'] = ingest_dict['accessPointId']
            else:
                raise (ValueError, 'accessPointId not found for the ingest')
            if 'errorMessage' in ingest_dict:
                self.__ingest_dict['errorMessage'] = ingest_dict['errorMessage']
            if 'stagingProcessor' in ingest_dict:
                sp_list = [int(i) for i in ingest_dict['stagingProcessor']]
                self.__ingest_dict['stagingProcessor'] = sp_list
            if 'stagingUrl' in ingest_dict:
                self.__ingest_dict['stagingUrl'] = ingest_dict['stagingUrl']

        def __init__(self, ingest_dict, kit_rest, group_id):
            self.__ingest_dict = dict()
            self.__ingest_dict['status'] = 0
            self.__ingest_dict['id'] = 0
            self.__ingest_dict['lastUpdate'] = 0
            self.__ingest_dict['expiresAt'] = 0
            self.__ingest_dict['digitalObjectUuid'] = None
            self.__ingest_dict['ownerUuid'] = None
            self.__ingest_dict['accessPointId'] = None
            self.__ingest_dict['errorMessage'] = None
            self.__ingest_dict['stagingUrl'] = None
            self.__ingest_dict['stagingProcessor'] = []
            self.__read_dict(ingest_dict)
            self.__kit_rest = kit_rest
            self.__group_access = group_id

        def __update_self(self):
            ingest_dict = get_ingest_dict(self.get_id(),
                                          get_my_group(),
                                          self.__url)
            self.__read_dict(ingest_dict)

        def get_id(self):
            return self.__ingest_dict['id']

        def get_staging_url(self):
            if self.__ingest_dict['stagingUrl'] == None:
                self.__update_self()
            return self.__ingest_dict['stagingUrl']

        def get_client_access_url(self):
            if self.__ingest_dict['clientAccessUrl'] == None:
                self.__update_self()
            return self.__ingest_dict['clientAccessUrl']

        def get_digital_object(self):
            do_Uuid = self.__ingest_dict['digitalObjectUuid']
            queries = _ReST_query_formatter([('doi', do_Uuid),
                                             ('groupId', get_my_group())])

            end_url = 'base/BaseMetaData/digitalObjects/doi'
            complete_url = REST_url + end_url + queries

            return digital_object(get_digital_object_dict(do_Uuid,
                                                          get_my_group(),
                                                          complete_url),
                                  complete_url)

        def get_access_point(self):
            amuuid = self.__ingest_dict['accessPointId']
            return aml.get_uuid(amuuid)

        def upload_file(self, local_file, remote_file):
            self.__update_self()

            if self.__ingest_dict['status'] != 4:
                raise (ImpossibleRequest_Error, ("This ingest is not ready "
                                                 "to receive data. "
                                                 "Look at its status!"))
            am = self.get_access_point()
            staging_url = self.__ingest_dict['stagingUrl']
            am.upload_file(staging_url + 'data/', local_file, remote_file)

        def upload_SEM_file(self, image_path, use_elasticsearch=None):
            # Raise: OSError if file hdf5 can not be written
            #        ElasticSearch_AlreadyStoredInfo_Error
            if use_elasticsearch == None:
                use_elasticsearch = __main__.use_elasticsearch
            if not os.path.isfile(image_path):
                raise ValueError('Not a valid file as image!')
            file_name = os.path.splitext(os.path.basename(image_path))[0]
            local_hdf5 = temp_folder + file_name
            for i in range(100):
                try:
                    image2hdf5(image_path, local_hdf5 + str(i) + '.hdf5', False)
                    local_hdf5 = local_hdf5 + str(i) + '.hdf5'
                    break
                except OSError:
                    pass
            else:
                raise OSError('File hdf5 can not be written!')

            try:
                # Calcolate sha checksum of image and hdf5
                image_hash = sha256_hash(image_path)
                hdf5_hash = sha256_hash(local_hdf5)

                upload_start_time = time.time()
                self.upload_file(local_hdf5, file_name + '.hdf5')
                upload_end_time = time.time()
            finally:
                os.remove(local_hdf5)

            my_do = self.get_digital_object()
            my_inv = my_do.get_investigation()
            my_st = my_inv.get_study()

            json_metadata = get_json_metadata(image_path,
                                              my_do.get_uuid(),
                                              my_inv.get_id(),
                                              my_st.get_id(),
                                              upload_start_time,
                                              upload_end_time,
                                              os.path.basename(local_hdf5),
                                              image_hash,
                                              hdf5_hash
                                              )
            print(json_metadata)
            if use_elasticsearch:
                # Search if some informations about this digital
                # object are already stored
                search_result = es.search(index=['_all'],
                                          q=my_do.get_uuid(),
                                          default_operator='AND')
                if search_result['hits']['total'] > 0:
                    raise ElasticSearch_AlreadyStoredInfo_Error('Informations for '
                                                                'an object with '
                                                                'the same UUID are '
                                                                'already stored in '
                                                                'Elasticsearch')
                es.create(index='sem_images',
                          body=json_metadata,
                          doc_type='sem_standard')

        def close(self):
            end_ingest(self.get_id())
            self.__update_self()

        def __repr__(self):
            return 'ingest_' + str(self.get_id())

        def __str__(self):
            output = ''
            for i in ['id', 'digitalObjectUuid', 'ownerUuid', 'status',
                      'stagingUrl', 'errorMessage', 'accessPointId',
                      'lastUpdate', 'expiresAt', 'stagingProcessor']:
                if self.__ingest_dict[i] != None:
                    output += _space_fill(i, 27)
                    output += str(self.__ingest_dict[i]) + '\n'
            return output


"""
if __name__ == '__main__':
    myself = RestUser('dama', 'USERS', 1, 'key', 'secret', 'admin', 'dama14')
    kit_rest = KitDMRestInterface('http://147.122.7.222:8080/KITDM/rest', myself)
    A = kit_rest.get_digital_object(5)
    #print(kit_rest._parse_access_point_xml(kit_rest.raw_get('staging/accesspoints/1', [('groupId','USERS')])))
    #print(kit_rest._get_access_point_dict(1))
    import remote_access_point_classes
    list_of_am_modules = []
    set_of_am_classes = []
    # Build a list of all modules inside the remote_access_point_classes package
    for importer, modname, ispkg in pkgutil.iter_modules(remote_access_point_classes.__path__):
        if not ispkg:
            list_of_am_modules.append(importlib.import_module('remote_access_point_classes.'+modname))
    #
    for mdl in list_of_am_modules:
        for name, obj in inspect.getmembers(mdl):
            if inspect.isclass(obj):
                print(obj)
                print(KitDMRestInterface.remote_access_point)
                if issubclass(obj, KitDMRestInterface.remote_access_point):
                    print('TROVATA')
"""


if __name__ == '__main__':

    myself = RestUser('dama', 'USERS', 1, 'key', 'secret', 'admin', 'dama14', 'dama', 'dama14')
    # myself = RestUser('dama', '2', 2, 'key', 'secret', 'admin', 'dama14')
    kit_rest = KitDMRestInterface('http://147.122.7.222:8080/KITDM/rest', myself)
    prova = IdrpClient('http://147.122.7.215:8080/idrp-service/rest/', myself)

    #register_on_idrp(kit_rest, prova, 57)

    #print "done"

    #exit()
    #prova.get_proposal(377)


    experiments_list = prova.get_proposal(377).get_experiments()
    for e in range(len(experiments_list)):
        print experiments_list[e]

    #prova.get_experiment('b1ceb66d-72e8-4e66-bbb4-818bf25a1ac5').create_measurement('RAW','measurement che funziona')

    measurement_list = prova.get_experiment('e855b319-6703-4129-94d1-c89019c7fc55').get_measurements()
    for m in range(len(measurement_list)):
        print measurement_list[m]

    #data_asset = [['THIS SHOULD NOT WORK', 'https://s-media-cache-ak0.pinimg.com/736x/75/df/01/75df01bb0da549b3ee30fe81068c87f0--ghibli-movies-kawaii-chibi.jpg', 'PUBLICATION']]
    #prova.get_measurement('a8a9cc7c-f63c-46f2-ad59-40cc9dca8e13').assign_data_assets(data_asset)
    print 'prova data assets ', prova.get_measurement('ecac3fb6-ed81-4f70-acfc-87eddb573e1d').get_data_assets()


    '''
    prova.get_proposal(377)
    #prova.get_experiment('a0780887-0c2e-4fb5-9059-76812052e5f7').create_measurement('RAW', 'measurement 3')
    measurement_list = prova.get_experiment('1d699188-6149-478d-b7fc-f02a3a86fd34').get_measurements()
    for m in range(len(measurement_list)):
        print measurement_list[m]



    #prova.get_experiment('a0780887-0c2e-4fb5-9059-76812052e5f7').create_measurement('RAW','measurement 3')
    measurement_list = prova.get_experiment('a0780887-0c2e-4fb5-9059-76812052e5f7').get_measurements()
    for m in range(len(measurement_list)):
        print measurement_list[m]

    print 'prova id ', prova.get_measurement('8d320e21-a3da-4dfd-8092-552217ce1f8c').get_id()

    data_asset_list = [['multiple d.a. 1', 'https://s-media-cache-ak0.pinimg.com/736x/92/18/85/921885925bf07d85d723ee267b2b2498.jpg', 'PLAIN_DATA'],
                       ['multiple d.a. 2', 'https://s-media-cache-ak0.pinimg.com/736x/75/df/01/75df01bb0da549b3ee30fe81068c87f0--ghibli-movies-kawaii-chibi.jpg',
                        'PUBLICATION']]

    #prova.get_measurement('a8a9cc7c-f63c-46f2-ad59-40cc9dca8e13').\
    #    create_data_asset('3rd data asset', 'https://s-media-cache-ak0.pinimg.com/736x/92/18/85/921885925bf07d85d723ee267b2b2498.jpg')

    prova.get_measurement('682ab4c6-872b-4afa-9cc2-bbb4e23855e1').assign_data_assets(data_asset_list)
    print 'prova data assets ', prova.get_measurement('682ab4c6-872b-4afa-9cc2-bbb4e23855e1').get_data_assets()

    #kit_rest = KitDMRestInterface('http://127.0.0.1:8081/KITDM/rest', myself)
    #B = kit_rest.get_staging_processor_id_list()
    #print('lista stagin processor', B)

    #for study in kit_rest.get_study_list():
    #    print study

    '''

    '''
    for study in kit_rest.get_study_list():
        for investigation in study.get_investigations():
            for object in investigation.get_digital_objects():
                #print(object)
                try:
                    print(object.get_id(), object.get_download_status()) #object.get_ingest_status() #, object.get_download_status()
                except:
                    print('no ingest')
                if object.get_ingest_id() is not None:
                    if object.get_ingest_status() < 16:
                        object.close_ingest()




    #hello = kit_rest.get_study(4).create_investigation('test_case').create_digital_object('test_object')
    hello = kit_rest.get_digital_object(37).get_investigation().create_digital_object('RIGHT_ONE')
    print(hello.get_id())
    #hello = kit_rest.get_digital_object(1).get_investigation().create_digital_object('prova')
    hello.schedule_ingest(kit_rest.get_access_point_list()[0])
    print('status = ', hello.get_ingest_status())
    local_file = '/home/rossella/data_folder/A66C10.hdf5'
    remote_file = 'A66C10.hdf5'
    #remote_file2 = 'NF-B_08.hdf5'
    #remote_file3 = 'glass_height_00.hdf5'
    #print("hello ingest url:", hello.get_ingest_url())
    hello.upload_file(local_file, remote_file, 'admin', 'dama14')
    print('status = ', hello.get_ingest_status()
    #hello.upload_file(local_file, remote_file)
    hello.close_ingest()
    print('status = ', hello.get_ingest_status())





    #hello.list_files()
    #hello.download_file(remote_file, os.path.join('/home/rossella/prova_all', remote_file))
    #hello.download_all('/home/rossella/prova_all')
    #hello.upload_file('/home/rossella/prova_all/Cells_on_Cambridge_Graphene_20.hdf5', 'Cells_on_Cambridge_Graphene_20.hdf5', 'admin', 'dama14')
    #hello.upload_all('/home/rossella/prova_all', 'admin', 'dama14')
    #hello.list_files()

    #A = kit_rest.get_digital_object(2)#.get_investigation().create_digital_object('gianni')
    # kit_rest.get_digital_object(1).get_investigation().create_digital_object('stefanino').get_ingest_id()
    #print("studio", A.get_investigation().get_study())
    #A.schedule_ingest(kit_rest.get_access_point_list()[0])
    #print(A.get_ingest_status())
    #for ap in kit_rest.get_access_point_list():
    #    print('ap', ap.get_name())

    #pippo = A.schedule_ingest(kit_rest.get_access_point_list()[0])
    #print('pippo', pippo)
    #print(kit_rest.raw_post('staging/ingests/', (),{"objectId": '7106c4d0-d65f-42cf-bdcc-3ac5bb608700',"accessPoint": '1'}))




    for study in kit_rest.get_study_list():
        for investigation in study.get_investigations():
            for object in investigation.get_digital_objects():
                #print(object)
                try:
                    print(object.get_id(), object.get_ingest_status()) #, object.get_download_status()
                except:
                    print('no ingest')

                if object.get_ingest_id() is None:
                    print object



    #hola = kit_rest.get_digital_object(8)
    #print hola.get_ingest_status()
    #hola.schedule_ingest(kit_rest.get_access_point_list()[0])
    #print hola.list_files()
    #hola.upload_file('/home/rossella/PycharmProjects/ciappi/A66C10.hdf5', 'A66C10.hdf5', 'admin', 'dama14')
    #hola.upload_all('/home/rossella/PycharmProjects/ciappi/folder', 'admin', 'dama14')
    #hola.close_ingest()
    #hola.download_file('A66C10.hdf5', 'A66C10.hdf5')
    #print(hola.get_download_id())
    #print(hola.get_download_status())
    #a = hola.list_files()

    #for i in a:
    #    print "element: ", i
    #content = hola.list_files()
    #for i in content:
    #    print(i)

    #webdav = easywebdav.connect('147.122.7.222', port=8080, username='admin', password='dama14',path='webdav/admin/5/data')
    #for file in webdav.ls():
    #    print(file)
    '''