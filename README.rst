This repo contains the Command Line Interface to the KIT Data Manager currently developed within the NFFA-Europe Project.

 - The **kit_rest_interface.py** contains all the functions needed by the interface.
   Up to now, this file is expected to be in the same directory of click.

 - The **config.ini** file contains the test credentials and all the settings to connect to the KIT-DM. 
   Up to now, this file is expected to be in the same directory of click.

 - The **click** (Command Line Interface Cnr-iom Kit) is the proper interface. 


Additional Packages needed
--------------------------

To make it work, please check if you have installed the following packages, 
and eventually install them with the following command::

  pip install easywebdav requests requests_oauthlib argparse argcomplete

**NB:** Argcomplete is not working on the Windows command prompt. 
On Windows 10, it is possible to use the bash::

https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/

In this case, everything works as you are on Ubuntu. To make argcomplete working::

	sudo activate-global-python-argcomplete
	
and restart the bash.



To start using it, type either::

	./click -h

or::

	python click -h


