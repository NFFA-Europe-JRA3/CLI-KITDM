#!/usr/bin/env python
# coding=utf-8
# PYTHON_ARGCOMPLETE_OK

'''
    Command line interface to KIT-DM.
    This file is part of click.
    click is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    click is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with click. If not, see <www.gnu.org/licenses/gpl.html>
'''

from kit_rest_interface import read_settings, ConnectionFailed, NotFound_Error, register_on_idrp
import argparse, argcomplete
import os


__author__ = "Rossella Aversa"
__copyright__ = "Copyright 2017, CNR-IOM within the NFFA-Europe project. " \
                "NFFA-Europe has received funding from the EU's H2020 framework programe for " \
                "research and innovation under grant agreement n. 654360"
__credits__ = ["Rossella Aversa", "Stefano Piani", "Stefano Cozzini"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Rossella Aversa"
__email__ = "aversa@iom.cnr.it"
__status__ = "Prototype"


if __name__ == '__main__':

    webdav, kit_rest, idrp_rest = read_settings('config.ini') # config file is assumed to be in the same dir of click
    interface_url = kit_rest._url.rstrip('/')

    parser = argparse.ArgumentParser(description="This is the command line interface to KIT-DM. "
                                                 "Created and mantained by Rossella Aversa. "
                                                 "Copyright 2017, CNR-IOM within the NFFA-Europe project. "
                                                 "NFFA-Europe has received funding from the EU's H2020 framework "
                                                 "programe for research and innovation under grant agreement n. 654360")
    subparsers = parser.add_subparsers(dest='ACTIONS')

    settings_parser = subparsers.add_parser('show_settings', help='show settings read from the config.ini file')
    show_parser = subparsers.add_parser('show_url', help='show the url for direct download of the digital object')
    list_parser = subparsers.add_parser('list', help='list studies, investigations, digital objects, files')
    upload_parser = subparsers.add_parser('upload', help='upload either a file or a folder')
    download_parser = subparsers.add_parser('download', help='download either a file, or the whole content of a digital '
                                                             'object as a zip file')
    create_parser = subparsers.add_parser('create', help='create a study, investigation, digital object')

    list_subparsers = list_parser.add_subparsers(dest='OBJECTS')
    upload_subparsers = upload_parser.add_subparsers(dest='OBJECTS')
    download_subparsers = download_parser.add_subparsers(dest='OBJECTS')
    create_subparsers = create_parser.add_subparsers(dest='OBJECTS')


    show_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                            help='id of the digital object for which you want the url')

    list_studies_parser = list_subparsers.add_parser('studies', help='list all the studies')

    list_investigations_parser = list_subparsers.add_parser('investigations',
                                                help='list the investigations. Default: all the investigations')
    list_investigations_parser.add_argument('-s', '--study', type=int,
                                            help='id of the study for which you want to list investigations')

    list_digital_objects_parser = list_subparsers.add_parser('digital_objects',
                                                help='list the digital objects. Default: all the digital objects.')
    list_group = list_digital_objects_parser.add_mutually_exclusive_group()
    list_group.add_argument('-i', '--investigation', type=int,
                            help='id of the investigation for which you want to list digital objects')
    list_group.add_argument('-s', '--study', type=int,
                            help='id of the study for which you want to list digital objects')

    list_files_parser = list_subparsers.add_parser('files', help='list the files in a digital object')
    list_files_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                   help='id of the digital object for which you want to list files')

    upload_file_parser = upload_subparsers.add_parser('file', help='upload a single file. '
                                                                   'Default: the remote file will have the same name '
                                                                   'of the local file')
    upload_file_parser.add_argument('-l', '--local', required=True, type=str, help='path of the local file you want to upload')
    upload_file_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                    help='id of the digital object in which you want to upload the file')
    upload_file_parser.add_argument('-r', '--remote', type=str, default=None,
                                   help='the name of the uploaded file in the remote destination '
                                        '(default: the same as the local file)')
    upload_folder_parser = upload_subparsers.add_parser('folder',
                                                        help='upload all the files contained in a folder. '
                                                             'The remote files will have the same name '
                                                             'of the local ones.')
    upload_folder_parser.add_argument('-l', '--local', required=True, type=str, help='path of the local folder you want to upload')
    upload_folder_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                      help='id of the digital object in which you want to upload the folder')

    download_file_parser = download_subparsers.add_parser('file', help='download a single file. '
                                                                       'Default: the local file will have the same name '
                                                                       'of the remote file and will be downloaded in '
                                                                       'the current directory')
    download_file_parser.add_argument('-r', '--remote', required=True, type=str, help='name of the remote file you want to download')
    download_file_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                      help='id of the digital object from which you want to download the file')
    download_file_parser.add_argument('-l', '--local', type=str, default=None,
                                      help='the path of the local destination, including the file name '
                                           '(default: current directory with the remote file name)')

    download_folder_parser = download_subparsers.add_parser('folder', help='download all the files in a digital object. '
                                                                           'Default: a zip file with the name of the '
                                                                           'digital object will be created in the '
                                                                           'current directory')
    download_folder_parser.add_argument('-d', '--digital_object', required=True, type=int,
                                        help='id of the digital object from which you want to download the files')
    download_folder_parser.add_argument('-l', '--local', type=str, default=None,
                                        help="the path of the local destination, including the name of the zip file. "
                                             "(default: a zip file 'DigitalObject' + the id of the digital object "
                                             "will be created in the current directory )")

    create_study_parser = create_subparsers.add_parser('study', help="create a new study.")
    create_study_parser.add_argument('-p', '--proposal', type=int, default=None, help="Proposal ID the study in related to.")
    create_study_parser.add_argument('-n', '--name', type=str, default=None, help="name of the study. WARNING: if you use this option, "
                                                                                "you WILL NOT be able to automatically register "
                                                                                "it into the IDRP!")

    create_investigation_parser = create_subparsers.add_parser('investigation', help="create a new investigation in "
                                                                                     "a given study.")
    create_investigation_parser.add_argument('-s', '--study', required=True, type=int, help='id of the study in which you want to create '
                                                                     'the new investigation')
    create_investigation_parser.add_argument('-n', '--name', type=str, help="name of the investigation (suggested: "
                                                                            "name of the experiment). "
                                                                            "If no name is specified, the generic name "
                                                                            "'investigation' will be assigned")

    create_digital_object_parser = create_subparsers.add_parser('digital_object', help="create a new digital object in "
                                                                                       "a given investigation.")
    create_digital_object_parser.add_argument('-i', '--investigation', required=True, type=int, help='id of the investigation in which you '
                                                                              'want to create the new digital object')
    create_digital_object_parser.add_argument('-n', '--name', type=str, help="name of the digital object "
                                                                             "(suggested: name of the measurement). "
                                                                             "If no name is specified, the generic "
                                                                             "name 'digital_object' will be assigned")

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    if args.ACTIONS =='show_settings':
        try:
            print('SETTINGS: ')
            print('username:', kit_rest._rest_user._name)
            print('user group:', kit_rest._rest_user._group)
            print('user id: ', kit_rest._rest_user._id)
            print('oauth authentication id: ', kit_rest._rest_user._auth_id)
            print('client secret: ', kit_rest._rest_user._cli_sec)
            print('resource owner key of oauth: ', kit_rest._rest_user._res_own_key)
            print('owner secret of oauth: ', kit_rest._rest_user._res_own_sec)
            print('kind of authentication: ', kit_rest._rest_user._sig_met)
            print('webdav username: ', webdav[0])
            print('webdav key: ', webdav[1])
            print('IDRP token: ', idrp_rest._rest_user._token)
            print('IDRP token secret: ', idrp_rest._rest_user._token_sec)
            print('KIT-DM interface url: ', kit_rest._url)
            print('IDRP interface url: ', idrp_rest._url)
        except Exception as e:
            print( e)


    if args.ACTIONS == 'show_url':
        try:
            selected_object = kit_rest.get_digital_object(args.digital_object)
        except Exception:
            print( 'Digital object {} does not exist!'.format(args.digital_object))
            exit()

        url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) + '/'
        print( 'URL for direct download: {}'.format(url))

    if args.ACTIONS == 'list':
        if args.OBJECTS == 'digital_objects':
            if args.investigation:
                try:
                    selected_investigation = kit_rest.get_investigation(args.investigation)
                except Exception:
                    print( 'Investigation {} does not exist!'.format(args.investigation))
                    exit()
                list = selected_investigation.get_digital_objects()
                if len(list) == 0:
                    print( 'Investigation {} has no digital objects!'.format(args.investigation))
                    exit()
                for object in list:
                    print( object)

            elif args.study:
                try:
                    selected_study = kit_rest.get_study(args.study)
                except Exception:
                    print( 'Study {} does not exist!'.format(args.study))
                    exit()
                investigation_list = selected_study.get_investigations()
                list = []
                for investigation in investigation_list:
                    object_list = investigation.get_digital_objects()
                    for object in object_list:
                        list.append(object)
                if len(list) == 0:
                    print( 'Study {} has no digital objects!'.format(args.study))
                    exit()
                for object in list:
                    print( object)

            else:
                list = []
                for study in kit_rest.get_study_list():
                    for investigation in study.get_investigations():
                        for object in investigation.get_digital_objects():
                            list.append(object)
                if len(list) == 0:
                    print( 'There are no digital objects!')
                    exit()
                for object in list:
                        print( object)

        elif args.OBJECTS == 'studies':
            study_list = kit_rest.get_study_list()
            if len(study_list) == 0:
                print( 'There are no studies!')
                exit()
            for study in study_list:
                print( study)

        elif args.OBJECTS == 'investigations':
            if args.study:
                try:
                    selected_study = kit_rest.get_study(args.study)
                except Exception:
                    print( 'Study {} does not exist!'.format(args.study))
                    exit()
                list = selected_study.get_investigations()
                if len(list) == 0:
                    print( 'Study {} has no investigations!'.format(args.study))
                    exit()
                for investigation in list:
                    print( investigation)

            else:
                list = []
                study_list = kit_rest.get_study_list()
                if len(study_list) == 0:
                    print( 'There are no studies!')
                    exit()
                for study in study_list:
                    investigation_list = study.get_investigations()
                    for investigation in investigation_list:
                        list.append(investigation)
                if len(list) == 0:
                    print( 'There are no investigations!')
                    exit()
                for investigation in list:
                    print( investigation)

        elif args.OBJECTS == 'files':
            try:
                selected_object = kit_rest.get_digital_object(args.digital_object)
            except Exception:
                print( 'Digital Object {} does not exist!'.format(args.digital_object))
                exit()

            try:
                list = selected_object.list_files()
                if len(list) == 0:
                    print('No files in digital object {}!'.format(selected_object.get_id()))
                    exit()
                print( 'Files in digital object {}:'.format(selected_object.get_id()))

                for file in list:
                    print(file)
            except ConnectionFailed as CF:
                status = selected_object.get_ingest_status()
                if status == 0:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST UNKNOWN!')
                elif status == 1:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST PREPARING. Please wait for the INGEST to finish!')
                elif status == 2:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST PREPARATION FAILED!')
                elif status == 4:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('PRE-INGEST SCHEDULED. Please wait for the INGEST to finish!')
                elif status == 8:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('PRE_INGEST_RUNNING. Please wait for the INGEST to finish!')
                elif status == 16:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('PRE-INGEST FINISHED. Please wait for the INGEST to finish!')
                elif status == 32:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('PRE-INGEST FAILED!')
                elif status == 64:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST RUNNING. Please wait for the INGEST to finish!')
                elif status == 128:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST FINISHED. Please wait!')
                elif status == 256:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), status))
                    print('INGEST_FAILED!')
                elif status == 512:
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), selected_object.get_ingest_status()))
                    print('INGEST_REMOVED!')
                else:
                    print(CF)
                    print('Ingest status for digital object {}: {}'.format(selected_object.get_id(), selected_object.get_ingest_status()))
                exit()
            except Exception as e:
                print(e)
                exit()

            url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) + '/'
            print( 'URL FOR DIRECT DOWNLOAD: {}'.format(url))


    elif args.ACTIONS == 'upload':
        if args.OBJECTS == 'file':
            if os.path.isfile(args.local) == False:
                print( "Local file '{}' does not exist! Check the path of your local file.".format(args.local))
                exit()

            if args.remote:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                try:
                    selected_object.schedule_ingest(kit_rest.get_access_point_list()[0])
                    print( 'schedulated ingest for digital object {} ({})'.format(args.digital_object,
                                                                                 selected_object.get_name()))
                    selected_object.upload_file(args.local, args.remote, webdav[0], webdav[1])
                    selected_object.close_ingest()
                    print( 'closed ingest for digital object {} ({})'.format(args.digital_object, selected_object.get_name()))
                except Exception as e:
                    selected_object.close_ingest()
                    print( e)
                    exit()

                try:
                    print('Trying to register on the IDRP...')
                    register_on_idrp(kit_rest, idrp_rest, args.digital_object)
                    print('Data Asset registered on the IDRP!')
                except Exception as e:
                    print(e)

            else:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                try:
                    remote = os.path.split(args.local)[1]
                    selected_object.schedule_ingest(kit_rest.get_access_point_list()[0])
                    print( 'schedulated ingest for digital object {} ({})'.format(args.digital_object,
                                                                                 selected_object.get_name()))
                    selected_object.upload_file(args.local, remote, webdav[0], webdav[1])
                    selected_object.close_ingest()
                    print( 'closed ingest for digital object {} ({})'.format(args.digital_object, selected_object.get_name()))
                except Exception as e:
                    print( e)
                    exit()

                try:
                    print('Trying to register on the IDRP...')
                    register_on_idrp(kit_rest, idrp_rest, args.digital_object)
                    print('Data Asset registered on the IDRP!')
                except Exception as e:
                    print(e)


        elif args.OBJECTS == 'folder':
            if os.path.isdir(args.local) == False:
                print( "Local folder '{}' does not exist! Check the path of your local folder.".format(args.local))
                exit()

            try:
                selected_object = kit_rest.get_digital_object(args.digital_object)
            except Exception:
                print( 'Digital Object {} does not exist!'.format(args.digital_object))
                exit()

            try:
                selected_object.schedule_ingest(kit_rest.get_access_point_list()[0])
                print( 'Schedulated ingest for digital object {} ({})'.format(args.digital_object,
                                                                             selected_object.get_name()))
                print ('Uploading files, please wait...')
                selected_object.upload_all(args.local, webdav[0], webdav[1])
                selected_object.close_ingest()
                print( 'Closed ingest for digital object {} ({})'.format(args.digital_object, selected_object.get_name()))
                print( "All files in '{}' have been uploaded to digital object {} ({})".format(args.local,
                                                                                          args.digital_object,
                                                                                          selected_object.get_name()))
            except Exception as e:
                selected_object.close_ingest()
                print(e)
                exit()

            try:
                print('Trying to register on the IDRP...')
                register_on_idrp(kit_rest, idrp_rest, args.digital_object)
                print('Data Asset registered on the IDRP!')
            except Exception as e:
                print(e)


    elif args.ACTIONS == 'download':
        if args.OBJECTS == 'file':
            if args.local:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                try:
                    selected_object.download_file(args.remote, args.local)
                except ConnectionFailed:
                    print( "Remote file '{}' does not exist in digital object {}!".format(args.remote, args.digital_object))
                    exit()
                except Exception as e:
                    print(e)
                    exit()

                url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) \
                      + '/' + str(args.remote)
                print( 'Succesfully downloaded!')
                print( 'URL FOR DIRECT DOWNLOAD: {}'.format(url))

            else:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                local = os.path.join(os.getcwd(), os.path.basename(args.remote))
                print( 'local path:{}'.format(local))

                try:
                    selected_object.download_file(args.remote, local)
                except ConnectionFailed:
                    print( "Remote file '{}' does not exist in digital object {}!".format(args.remote,
                                                                                         args.digital_object))
                    exit()

                url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) \
                      + '/' + str(args.remote)
                print( 'Succesfully downloaded!')
                print( 'URL FOR DIRECT DOWNLOAD: {}'.format(url))

        elif args.OBJECTS == 'folder':
            if args.local:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                try:
                    selected_object.download_zip(args.local)
                except Exception:
                    print( 'Never uploaded anythin to digital object {}'.format(selected_object.get_id()))
                    exit()

                url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) \
                      + '/'
                print( 'Succesfully downloaded!')
                print('URL FOR DIRECT DOWNLOAD: {}'.format(url))

            else:
                try:
                    selected_object = kit_rest.get_digital_object(args.digital_object)
                except Exception:
                    print( 'Digital Object {} does not exist!'.format(args.digital_object))
                    exit()

                local = os.path.join(os.getcwd(), 'DigitalObject' + str(args.digital_object) + '.zip')

                try:
                    selected_object.download_zip(local)
                except Exception:
                    print( 'Never uploaded anythin to digital object {}'.format(selected_object.get_id()))
                    exit()

                url = str(interface_url) + '/dataorganization/organization/download/' + str(selected_object.get_id()) \
                      + '/'
                print( 'Succesfully downloaded!')
                print( 'URL FOR DIRECT DOWNLOAD: {}'.format(url))

    elif args.ACTIONS == 'create':
        if args.OBJECTS == 'study':
            print 'proposal=', args.proposal
            print 'name=', args.name
            # check wether the name given to the study is a generic name or is related to a proposal
            if args.proposal is None and args.name is None:
                print("Please indicate a name either for the study or the NFFA proposal ID it is related to!")
                exit()
            elif args.proposal is not None and args.name is not None:
                print("Name and Proposal are mutually exclusive options. Please indicate only one of them!")
                exit()
            elif args.proposal is None and args.name is not None:
                new_study = kit_rest.create_study(args.name)
                print("New study '{}' created with id = {}".format(new_study.get_name(), new_study.get_id()))
                print("WARNING: using this option, the automatic registration into the IDRP is NOT possible!")
            elif args.proposal is not None and args.name is None:
                # check whether a valid proposal exists. If so, create the study
                try:
                    proposal = idrp_rest.get_proposal(args.proposal)
                    new_study = kit_rest.create_study('NFFA_Proposal_' + str(args.proposal))
                    print( "New study '{}' created with id = {}".format(new_study.get_name(), new_study.get_id()))
                except NotFound_Error:
                    print("No NFFA proposal found with this ID!")
                    exit()


        elif args.OBJECTS == 'investigation':
            try:
                selected_study = kit_rest.get_study(args.study)
                print( "Selected study: '{}' with id = {}".format(selected_study.get_name(), args.study))
            except Exception:
                print( 'Study {} does not exist!'.format(args.study))
                exit()

            if args.name:
                new_investigation = selected_study.create_investigation(args.name)
                print( "New investigation '{}' created with id = {}".format(args.name, new_investigation.get_id()))
            else:
                new_investigation = selected_study.create_investigation('investigation')
                print( "New investigation 'investigation' created with id = {}".format(new_investigation.get_id()))

        elif args.OBJECTS == 'digital_object':
            try:
                selected_investigation = kit_rest.get_investigation(args.investigation)
                print( "Selected investigation: '{}' with id = {}".format(selected_investigation.get_name(), args.investigation))
            except Exception:
                print( 'Investigation {} does not exist!'.format(args.investigation))
                exit()

            if args.name:
                new_digital_object = selected_investigation.create_digital_object(args.name)
                print( "New digital object '{}' created with id = {}".format(args.name, new_digital_object.get_id()))
            else:
                new_digital_object = selected_investigation.create_digital_object('digital_object')
                print( "New digital object 'digital_object' created with id = {}".format(new_digital_object.get_id()))
